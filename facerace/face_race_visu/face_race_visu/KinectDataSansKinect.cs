﻿/*
 This is a dummy kinect class, where you can control the kinect with the keyboard.
 * 
 * Be sure to call handleKeyStrokes on every update!
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

//kinect stuff 
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using Microsoft.Kinect.Toolkit.FaceTracking;

namespace face_race_visu
{
    class KinectDataSansKinect : KinectDataInterface
    {

        Texture2D fakeColorData = null;

        public KinectDataSansKinect()
        {
            trackedSkeletons.Add(0, new SkeletonFaceTracker(0));
            trackedSkeletons.Add(1, new SkeletonFaceTracker(1));

            get_player(0).get_kinect_face().mIsTracked = true;
            get_player(0).get_kinect_face().mIsTracked = true;
        }

        public void set_kinect(KinectSensor aSensor) 
        {
        }

        private const uint MaxMissedFrames = 100;

        public byte[] colorImage;
        public ColorImageFormat colorImageFormat = ColorImageFormat.Undefined;
        public short[] depthImage;
        public DepthImageFormat depthImageFormat = DepthImageFormat.Undefined;

        public Skeleton[] skeletonData;
        public readonly Dictionary<int, SkeletonFaceTracker> trackedSkeletons = new Dictionary<int, SkeletonFaceTracker>();

        public int getNTrackedPlayers()
        {
            return 2;

        }


        public SkeletonFaceTracker get_player(int aPlayerNumber)
        {
            return trackedSkeletons.Count > aPlayerNumber ? trackedSkeletons.ElementAt(aPlayerNumber).Value : null;

            //Dictionary<int, SkeletonFaceTracker>.Enumerator rator = trackedSkeletons.GetEnumerator();
            //for (int i = 0; i < aPlayerNumber; i++)
            //{
            //    rator.MoveNext();
            //}
            //return rator.Current.Value;
        }

        public Texture2D get_color_texture(GraphicsDevice aDevice)
        {
            if (fakeColorData == null)
            {
                fakeColorData = new Texture2D(aDevice, 640, 480); 
            }
            return fakeColorData;
        }
        /*{
            if (colorImage != null)
            {
                Texture2D r;
                switch (colorImageFormat)
                {
                    case ColorImageFormat.RawYuvResolution640x480Fps15:
                    case ColorImageFormat.RgbResolution640x480Fps30:
                    case ColorImageFormat.YuvResolution640x480Fps15:
                        r = new Texture2D(aDevice, 640, 480,false,SurfaceFormat.Color);
                        break;
                    default:
                        throw new Exception("Kinect ColorImageFormat not handled");
                }  
                r.SetData<byte>(colorImage);
                return r;   
            }
            return null;
        }
        */
        public void KinectSensorOnAllFramesReady(object sender, AllFramesReadyEventArgs allFramesReadyEventArgs) 
        {
            this.colorImage = new byte[640*480*4];
        }

        public void handleKeyStrokes()
        {
            KeyboardState ks = Keyboard.GetState();



            if (ks.IsKeyDown(Keys.A)) //open p1 mouth
            {
                trackedSkeletons[0].get_kinect_face().add_AU_frames(new float[] { 1, 1, 1, 1, 1, 1 });
            }
            else
            {
                 trackedSkeletons[0].get_kinect_face().add_AU_frames(new float[] { 0, 0, 0, 0, 0, 0 });
            }

            if (ks.IsKeyDown(Keys.B))
            {
                trackedSkeletons[1].get_kinect_face().add_AU_frames(new float[] { 1, 1, 1, 1, 1, 1 });
            }
            else
            {
                trackedSkeletons[1].get_kinect_face().add_AU_frames(new float[] { 0, 0, 0, 0, 0, 0 });
            }
        }

    }
}
