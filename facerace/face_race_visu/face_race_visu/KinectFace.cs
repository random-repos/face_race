﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
namespace face_race_visu
{

    //this class represents a either tracked or untracked face
    public class KinectFace
    {
        public int mSkeletonTrackingId = -1;
        public bool mIsTracked = false;
        public Vector2 mCenter = new Vector2();
        public Vector3 Rotation = new Vector3();
        //head angle
        //AUs
        //SUs?
        //special points (e.g. 2 an 6 tell you if left eye is closed or not)

        List<SmoothedSignal> mAUs;

        public KinectFace()
        {
            mAUs = new List<SmoothedSignal>();
            for (int i = 0; i < 6; i++)
            {
                mAUs.Add(new SmoothedSignal());
                mAUs[i].set_history_length(1);
            }
        }
        public void add_AU_frames(float[] aAUs)
        {
            if (aAUs.Length != 6) 
                throw new Exception("AUs not length 6");
            for (int i = 0; i < aAUs.Length; i++)
            {
                mAUs[i].add_value(aAUs[i]);
            }
        }
        public float[] get_smoothed_AUs()
        {
            float[] r = new float[mAUs.Count];
            for (int i = 0; i < mAUs.Count; i++)
            {
                r[i] = mAUs[i].get_average();
            }
            return r;
        }
    }

    //this has additional information concerning the user
    class LearnedFace : KinectFace
    {
     
    }
}
