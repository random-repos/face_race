using System;

namespace face_race_visu
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            //using (PhysicsGame game = new PhysicsGame())
            //using (Learning game = new Learning())
            //using (ClassExamples game = new ClassExamples())
            
            //using (Microsoft.Xna.Framework.Game game = new GameQuickDraw())
            using (Microsoft.Xna.Framework.Game game = new GameHeadMovingBiting())
            //using (Microsoft.Xna.Framework.Game game = new GamePelletStreamEater())
            //using (Microsoft.Xna.Framework.Game game = new GameBasketBall())
            {
                game.Run();
            }
        }
    }
#endif
}

