﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


//kinect stuff 
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;

namespace face_race_visu
{
    public class GamePelletStreamEater : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont mDefaultFont;
        BasicEffect mBasicEffect;
        KinectData mKinect = new KinectData();


        //kinect crap
        private static readonly int Bgr32BytesPerPixel = (System.Windows.Media.PixelFormats.Bgr32.BitsPerPixel + 7) / 8;
        private readonly KinectSensorChooser sensorChooser = new KinectSensorChooser();


        //eb
        class Edible : Sprite
        {
            public enum RequiredResponse
            {
                CLOSE = 0,
                OPEN = 1,
                CHEW = 3,
            }

            public float mDuration;
            public RequiredResponse mResponseNeeded;
            public Edible(Vector2 aPos, RequiredResponse aType, float aDuration = 0) : base(aPos)
            {
                mPos = aPos;
                mResponseNeeded = aType;
                mDuration = aDuration;
            }
        }


        enum gameState { FINDING_FACES, PLAYING, LOSE };
        gameState ebGameState = gameState.PLAYING;
        int ebTicksElapsed = 0;
        int ebHitPoints = 10;
        bool ebHasFace = false;
        List<Edible> mEdibles = new List<Edible>();
        Edible mInside = null;
        Vector2 mFaceCenter = new Vector2(0,0);
        Texture2D mOpenTexture;
        Texture2D mCloseTexture;
        Texture2D mImgOPEN;
        Texture2D mImgCLOSE;
        Texture2D mImgCHEW;
        SoundEffect mSoundCorrect;
        SoundEffect mSoundWrong;
        SoundEffect mSoundEat;
        SoundEffect mSoundSwallow;
        SoundEffect mSoundLose;

        public GamePelletStreamEater()
        {
            graphics = new GraphicsDeviceManager(this);
            //graphics.ToggleFullScreen();
            Window.Title = "NOTUNPACMAN";
            graphics.PreferMultiSampling = true;
            Content.RootDirectory = "Content";
            
            /**
            int spacer = 0;
            int dist = 350;
            mEdibles.Add(new Edible(new Vector2(500 + spacer, 0), Edible.RequiredResponse.CHEW));

            spacer += dist;
            mEdibles.Add(new Edible(new Vector2(500 + spacer, 0), Edible.RequiredResponse.OPEN));

            spacer += dist;
            mEdibles.Add(new Edible(new Vector2(500 + spacer, 0), Edible.RequiredResponse.CLOSE));

            spacer += dist;
            mEdibles.Add(new Edible(new Vector2(500 + spacer, 0), Edible.RequiredResponse.OPEN));*/
        }

        protected override void Initialize()
        {
            base.Initialize();
            graphics.PreferredBackBufferHeight = 480;//GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            graphics.PreferredBackBufferWidth = 640;// GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            graphics.ApplyChanges();
            this.IsMouseVisible = true;
            Window.AllowUserResizing = true;

            //2d
            mBasicEffect = new BasicEffect(graphics.GraphicsDevice);
            mBasicEffect.VertexColorEnabled = true;
            mBasicEffect.Projection = Matrix.CreateOrthographicOffCenter
               (0, graphics.GraphicsDevice.Viewport.Width,     // left, right
                graphics.GraphicsDevice.Viewport.Height, 0,    // bottom, top
                0, 1);

            //kinect
            sensorChooser.KinectChanged += SensorChooserOnKinectChanged;
            sensorChooser.Start();

            mFaceCenter = new Vector2(graphics.PreferredBackBufferWidth / 5, graphics.PreferredBackBufferHeight / 2);
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            mDefaultFont = this.Content.Load<SpriteFont>("DefaultFont");
            mOpenTexture = this.Content.Load<Texture2D>("cc_open");
            mCloseTexture = this.Content.Load<Texture2D>("cc_closed");
            mImgOPEN = this.Content.Load<Texture2D>("bustersalmon");
            mImgCLOSE = this.Content.Load<Texture2D>("glassfrogA");
            mImgCHEW = this.Content.Load<Texture2D>("blueringoctopus");
            mSoundCorrect = this.Content.Load<SoundEffect>("bbeep");
            mSoundWrong = this.Content.Load<SoundEffect>("boop");
            mSoundEat = this.Content.Load<SoundEffect>("bbeep");
            mSoundSwallow = this.Content.Load<SoundEffect>("bbeep");
            mSoundLose = this.Content.Load<SoundEffect>("bbeep");
        }

        protected override void UnloadContent() { }

        protected override void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();
            if (ks.IsKeyDown(Keys.Escape))
            {
                this.Exit();
            }

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                this.Exit();
            }

            eb_update();

            base.Update(gameTime);


        }
        public void eb_reset_game()
        {
        }
        public void eb_lose_health()
        {
            ebHitPoints -= 1;
            mSoundWrong.Play();
            //TODO render explosion
        }
        public void eb_update()
        {

            bool everythingOK = true;
            if (mKinect.trackedSkeletons.Count > 0)
            {

                for (int i = 0; i < 1; i++)
                {
                    SkeletonFaceTracker e = mKinect.get_player(i);//mKinect.trackedSkeletons.Values.ElementAt(i);
                    if (e == null)
                    {
                        everythingOK = false;
                    }
                }
            }
            else everythingOK = false;
            if (!everythingOK)
            {
                ebHasFace = false;
                ebGameState = gameState.FINDING_FACES;
            }
            else
            {
                ebHasFace = true;
                ebGameState = gameState.PLAYING;
            }

            if (ebGameState == gameState.PLAYING) // Wait for stable expression
            {

                //Generate more edibles, if not enough in the queue
                if (mEdibles.Count() == 0)
                {
                    Random r = new Random();
                    int nNewEdibles = r.Next(3, 7);
                    int spacing = 50 + 50 * r.Next(0, 4);
                    for (int i = 0; i < nNewEdibles; i++)
                    {
                        int edibleTypeNum =  r.Next(1, 4);
                        
                        Edible.RequiredResponse edibleType;
                        
                        if (edibleTypeNum == 1)
                        {
                            edibleType = Edible.RequiredResponse.CHEW;
                            //edibleType = Edible.RequiredResponse.OPEN;
                        }
                        else if (edibleTypeNum == 2)
                        {
                            edibleType = Edible.RequiredResponse.CLOSE;
                        }
                        else //if (edibleType == 3)
                        {
                            edibleType = Edible.RequiredResponse.CHEW;
                        }


                        mEdibles.Add(new Edible(new Vector2(500 + i*spacing, 0), edibleType));
                    }
                }

                ebTicksElapsed++;
                
                bool mouthIsOpen = mouthOpen(0);

                List<Edible> toRemove = new List<Edible>();

                if (mInside != null && !mouthIsOpen) //something in mouth, and close mouth
                {
                    //toRemove.Add(mInside);
                    mEdibles.Remove(mInside);
                    mInside = null;
                    mSoundSwallow.Play();
                }

                foreach (Edible e in mEdibles)
                {

                    if (e.mPos.X > 0)
                    {
                        e.mPos.X -= 2;
                        e.mPos.X = Math.Max(0, e.mPos.X);
                    }
                    else
                    {
                        //if (mInside != null)
                        //{
                        //    eb_lose_health();
                        //}

                        if (e.mResponseNeeded == Edible.RequiredResponse.CHEW)
                        {
                            if (mInside != null && mInside != e) //already have something in mouth
                            {
                                toRemove.Add(mInside);
                                eb_lose_health();
                            }

                            if (!mouthIsOpen) // pellet hits face
                            {
                                eb_lose_health();
                                toRemove.Add(e);
                            }
                            else // pellet enters mouth
                            {
                                mInside = e;
                            }
                        }
                        else if (e.mResponseNeeded == Edible.RequiredResponse.OPEN)
                        {

                            if (mInside != null) //pellet hits other pellet already in mouth
                            {
                                toRemove.Add(mInside);
                                eb_lose_health();
                                mInside = null;
                            }

                            if (!mouthIsOpen) // pellet hits face
                            {
                                eb_lose_health();
                                //mEdibles.Remove(e);
                            }
                            else //pellet enters mouth, is swallowed
                            {
                                mSoundSwallow.Play();
                            }
                            toRemove.Add(e);
                        }
                        else if (e.mResponseNeeded == Edible.RequiredResponse.CLOSE)
                        {

                            if (mInside != null) //pellet hits other pellet already in mouth
                            {
                                toRemove.Add(mInside);
                                eb_lose_health();
                                mInside = null;
                            }
                            else if (mouthIsOpen) ////pellet enters mouth (wrong)
                            {
                                eb_lose_health();
                                //mEdibles.Remove(e);
                            }
                            else // pellet strikes face (correct)
                            {
                                mSoundCorrect.Play();
                            }
                            toRemove.Add(e);
                        }
                        
                        //e.mDuration -= 1;
                        //if (e.mType == Edible.EdibleTypes.CLOSE && e.mDuration < 0)
                        //{
                        //    mEdibles.Remove(e);
                        //    toRemove.Add(e);
                        //}
                    }
                }

                foreach (Edible e in toRemove)
                {
                    mEdibles.Remove(e);
                }

                if (ebHitPoints == 0)
                    ebGameState = gameState.LOSE;
            }
            else if (ebGameState == gameState.LOSE) // Tell player what expression to make
            {
                eb_reset_game();
                //TODO tell the player they lost
                ebGameState = gameState.PLAYING;
            }
        }

        protected Boolean mouthOpen(int playerIndex)
        {
            return ebHasFace && mKinect.get_player(playerIndex).get_kinect_face().get_smoothed_AUs()[1] > .3f;
        }

        public void eb_draw()
        {
            //EYEBROW GAME
            spriteBatch.Begin();
            Vector2 drawSize = new Vector2(200,200);

            foreach (Edible e in mEdibles)
            {
                Vector2 loc = new Vector2(e.mPos.X + mFaceCenter.X + 20, e.mPos.Y + mFaceCenter.Y + 25);
                Vector2 dim = new Vector2(75,75);
                Rectangle rect = new Rectangle((int)(loc.X-dim.X/2),(int)(loc.Y-dim.Y/2),(int)(dim.X),(int)(dim.Y));
                String pelletAppearanceString = "";
                if (e.mResponseNeeded == Edible.RequiredResponse.OPEN)
                {
                    pelletAppearanceString = "[O]";
                    spriteBatch.Draw(mImgOPEN, rect, Color.White);
                }
                else if (e.mResponseNeeded == Edible.RequiredResponse.CHEW)
                {
                    pelletAppearanceString = "[()]";
                    spriteBatch.Draw(mImgCHEW, rect, Color.White);
                }
                else if (e.mResponseNeeded == Edible.RequiredResponse.CLOSE)
                {
                    pelletAppearanceString = "{-}";
                    spriteBatch.Draw(mImgCLOSE, rect, Color.White);
                }

                spriteBatch.DrawString(mDefaultFont, pelletAppearanceString, new Vector2(e.mPos.X + mFaceCenter.X, e.mPos.Y + mFaceCenter.Y + 25), Color.Black);
            }

            if (mouthOpen(0))
            {
                spriteBatch.Draw(mOpenTexture, new Rectangle((int)(mFaceCenter.X - drawSize.X / 2), (int)(mFaceCenter.Y - drawSize.Y / 2), (int)(drawSize.X), (int)(drawSize.Y)), Color.White);
                //spriteBatch.DrawString(mDefaultFont, "P1 -> :O", new Vector2(300, 300), Color.Black);
            }
            else
            {
                spriteBatch.Draw(mCloseTexture, new Rectangle((int)(mFaceCenter.X - drawSize.X / 2), (int)(mFaceCenter.Y - drawSize.Y / 2), (int)(drawSize.X), (int)(drawSize.Y)), Color.White);
            }

            String healthBar = "";
            for (int i = 0; i < ebHitPoints; i++ )
            {
                healthBar = healthBar + "<3 ";
            }

             spriteBatch.DrawString(mDefaultFont, healthBar, new Vector2(15, 35), Color.Red);

            spriteBatch.End();
        }


        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            int fontHeightIncrement = 30;
            int fontHeight = 10;
            spriteBatch.Begin();
            Texture2D t2d = mKinect.get_color_texture(graphics.GraphicsDevice);
            if (t2d != null)
            {
                spriteBatch.Draw(t2d, new Rectangle(0, 0, 640, 480), Color.White);
                spriteBatch.DrawString(mDefaultFont, "Tracking working", new Vector2(10, fontHeight), Color.Black);
                fontHeight += fontHeightIncrement;
                spriteBatch.DrawString(mDefaultFont, "Faces found: " + mKinect.trackedSkeletons.Count, new Vector2(10, fontHeight), Color.Black);
            }

            for (int i = 0; i < 2; i++) //for each player
            {
                if (mKinect.get_player(i) != null)
                {
                    VertexPositionColor[] vertices = mKinect.get_player(i).get_2d_face_model();
                    KinectFace playerFace = mKinect.get_player(i).get_kinect_face();
                    if (vertices != null)
                    {
                        fontHeight += fontHeightIncrement;
                        string s = "";
                        foreach (float f in playerFace.get_smoothed_AUs())
                            s += " " + (f >= 0 ? "+" : "") + f.ToString("0.00");
                        spriteBatch.DrawString(mDefaultFont, "AU: " + s, new Vector2(10, fontHeight), Color.Black);
                        mBasicEffect.CurrentTechnique.Passes[0].Apply();
                        graphics.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineStrip, vertices, 0, vertices.Length / 2);
                    }
                    spriteBatch.DrawString(mDefaultFont, "P" + (i + 1) + "", playerFace.mCenter, Color.BlueViolet);
                }
            }

            //Draw debug emoticons
            if (mKinect.get_player(0) != null)
            {
                if (mouthOpen(0))
                {
                    spriteBatch.DrawString(mDefaultFont, "P1 -> :O", new Vector2(10, 400), Color.Black);
                }
                else
                {
                    spriteBatch.DrawString(mDefaultFont, "P1 -> :|", new Vector2(10, 400), Color.Black);
                }
            }

            if (mKinect.get_player(1) != null)
            {
                if (mouthOpen(1))
                {
                    spriteBatch.DrawString(mDefaultFont, ":O <- P2", new Vector2(590, 400), Color.Black);
                }
                else
                {
                    spriteBatch.DrawString(mDefaultFont, ":| <- P2", new Vector2(590, 400), Color.Black);
                }
            }

            spriteBatch.End();
            eb_draw();
            base.Draw(gameTime);
        }


        public void render_message(SpriteBatch asb, SpriteFont asf, string amsg)
        {
            asb.DrawString(asf, amsg, new Vector2(100, 200), Color.Red);
        }

        private void SensorChooserOnKinectChanged(object sender, KinectChangedEventArgs kinectChangedEventArgs)
        {
            KinectSensor oldSensor = kinectChangedEventArgs.OldSensor;
            KinectSensor newSensor = kinectChangedEventArgs.NewSensor;

            if (oldSensor != null)
            {
                oldSensor.AllFramesReady -= mKinect.KinectSensorOnAllFramesReady;
                oldSensor.ColorStream.Disable();
                oldSensor.DepthStream.Disable();
                oldSensor.DepthStream.Range = DepthRange.Default;
                oldSensor.SkeletonStream.Disable();
                oldSensor.SkeletonStream.EnableTrackingInNearRange = false;
                oldSensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Default;
            }

            if (newSensor != null)
            {
                try
                {
                    newSensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);
                    newSensor.DepthStream.Enable(DepthImageFormat.Resolution320x240Fps30);
                    try
                    {
                        // This will throw on non Kinect For Windows devices.
                        newSensor.DepthStream.Range = DepthRange.Near;
                        newSensor.SkeletonStream.EnableTrackingInNearRange = true;
                    }
                    catch (InvalidOperationException)
                    {
                        newSensor.DepthStream.Range = DepthRange.Default;
                        newSensor.SkeletonStream.EnableTrackingInNearRange = false;
                    }

                    newSensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;
                    newSensor.SkeletonStream.Enable();
                    newSensor.AllFramesReady += mKinect.KinectSensorOnAllFramesReady;
                    mKinect.set_kinect(newSensor);
                }
                catch (InvalidOperationException)
                { }
            }
        }
    }
}
