﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace face_race_visu
{
    class SmoothedSignal
    {
        Queue<float> mData = new Queue<float>();
        int mLength = 1;
        public float get_average() 
        { 
            if (mData.Count > 0) 
                return mData.Average(); 
            else return 0; 
        }
        public void add_value(float aValue)
        {
            mData.Enqueue(aValue);
            while (mData.Count > mLength)
                mData.Dequeue();
        }

        public void set_history_length(int aLength)
        {
            mLength = aLength;
        }
    }
}
