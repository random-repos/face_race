﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace face_race_visu
{

    public class Sprite
    {
        public bool dead = false;
        public Vector2 mPos;
        public delegate bool KillCondition(Sprite s);
        protected List<KillCondition> killConditions = null;



        public Sprite(Vector2 aPos)
        {
            mPos = aPos;
        }

        public virtual void update(GameTime time = null)
        {
            if (killConditions != null)
            {
                foreach( KillCondition kc in killConditions)
                {
                    this.dead |= kc(this);
                }
            }
        }

        public void draw()
        {
            draw(new Camera(GameHeadMovingBiting.get_window_dimensions()));
        }
        public virtual void draw(Camera cam)
        {
        }

        public void addKillCondition(KillCondition kc)
        {
            if (killConditions == null)
            {
                killConditions = new List<KillCondition>();
            }
            killConditions.Add(kc);
        }
    }

    public class BasicTextureSprite : Sprite
    {
        Texture2D mImg = null;
        protected float imgScale = 1.0f;
        protected bool flip = false;

        public BasicTextureSprite(Vector2 aPos, Texture2D img = null)
            : base(aPos)
        {
            mImg = img;
        }

        public override void draw(Camera cam)
        {
            if (mImg != null)
            {
                Vector2 loc = mPos;
                Vector2 dim = new Vector2(imgScale * mImg.Width, imgScale * mImg.Height);
                Rectangle rect = new Rectangle((int)(loc.X - dim.X / 2), (int)(loc.Y - dim.Y / 2), (int)(dim.X), (int)(dim.Y));
                rect = cam.transform_rectangle(rect);
                GameHeadMovingBiting.spriteBatch.Draw(mImg, rect, null, Color.White, 0, new Vector2(0, 0), flip ? SpriteEffects.FlipHorizontally : 0, 0);
            }
        }
    }


    public class AnimatedSprite : Sprite
    {
        float mTimer = 0;
        float mFramesPerSeconds;
        protected SpriteGraphicsInterface mSprite = null;
        protected bool mFlip = false;
        public AnimatedSprite(Vector2 aPos, float aFramesPerSecond, SpriteGraphicsInterface aSprite)
            : base(aPos)
        {
            mFramesPerSeconds = aFramesPerSecond;
            mSprite = aSprite;
        }
        public int compute_index()
        {
            return (int)(mFramesPerSeconds * mTimer) % mSprite.get_frame_count();
        }
        public override void update(GameTime time)
        {
            mTimer += (float)time.ElapsedGameTime.TotalSeconds;
            base.update(time);
        }
        public override void draw(Camera cam)
        {
            //if (cam == null)    draw(new Camera());
            if (mSprite != null)
            {
                Rectangle cropRect;
                Texture2D tex;
                mSprite.get_sprite_graphics(compute_index(), out cropRect, out tex);
                Rectangle drawRect = new Rectangle((int)(mPos.X - cropRect.Width / 2), (int)(mPos.Y - cropRect.Height / 2), cropRect.Width, cropRect.Height);
                drawRect = cam.transform_rectangle(drawRect);
                GameHeadMovingBiting.spriteBatch.Draw(tex, drawRect, cropRect, Color.White, 0, new Vector2(0, 0), mFlip ? SpriteEffects.FlipHorizontally : 0, 0);
            }

        }
    }
}