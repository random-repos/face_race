﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


//kinect stuff 
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;


// Physics
using Box2D.XNA;

namespace face_race_visu
{
    public class PhysicsGame : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont mDefaultFont;

        // kinect stuff
        BasicEffect mBasicEffect;
        KinectData mKinect = new KinectData();

        //kinect crap
        private static readonly int Bgr32BytesPerPixel = (System.Windows.Media.PixelFormats.Bgr32.BitsPerPixel + 7) / 8;
        private readonly KinectSensorChooser sensorChooser = new KinectSensorChooser();


        //eb
        enum gameState { FINDING_FACES, PLAYING, LOSE };
        gameState ebGameState = gameState.PLAYING;
        int ebHitPoints = 10;
        bool ebHasFace = false;
        Vector2 mFaceCenter = new Vector2(0, 0);
        Texture2D mBallTexture;
        Texture2D mPlayerTexture;

        World mPhysicsWorld;
        Body mPhysicsBall;
        Body mPhysicsPlayer;

        const float pixelsPerMeter = 50;

        float PixelsToMeters(int pixels)
        {
            return (float)pixels / pixelsPerMeter;
        }

        Vector2 PixelsToMeters(Vector2 pixels)
        {
            return new Vector2(PixelsToMeters((int)pixels.X), PixelsToMeters((int)pixels.Y));
        }
        
        int MetersToPixels(float meters)
        {
            return (int)(meters * pixelsPerMeter);
        }

        Vector2 MetersToPixels(Vector2 meters)
        {
            return new Vector2(MetersToPixels(meters.X), MetersToPixels(meters.Y));
        }

        public PhysicsGame()
        {
            graphics = new GraphicsDeviceManager(this);

            Window.Title = "Goo Volleyball";
            graphics.PreferMultiSampling = true;
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();
            graphics.PreferredBackBufferHeight = 480;
            graphics.PreferredBackBufferWidth = 640;
            graphics.ApplyChanges();
            this.IsMouseVisible = true;
            Window.AllowUserResizing = true;

            //2d
            mBasicEffect = new BasicEffect(graphics.GraphicsDevice);
            mBasicEffect.VertexColorEnabled = true;
            mBasicEffect.Projection = Matrix.CreateOrthographicOffCenter
               (0, graphics.GraphicsDevice.Viewport.Width,     // left, right
                graphics.GraphicsDevice.Viewport.Height, 0,    // bottom, top
                0, 1);

            //kinect
            sensorChooser.KinectChanged += SensorChooserOnKinectChanged;
            sensorChooser.Start();

            mFaceCenter = new Vector2(graphics.PreferredBackBufferWidth / 5, graphics.PreferredBackBufferHeight / 2);

            mPhysicsWorld = new World(new Vector2(0, 9.8f), true);

            // add walls
            float bbwidth = PixelsToMeters(GraphicsDevice.PresentationParameters.BackBufferWidth);
            float bbheight = PixelsToMeters(GraphicsDevice.PresentationParameters.BackBufferHeight);
            
            Body wall = mPhysicsWorld.CreateBody(new BodyDef());
            EdgeShape shape = new EdgeShape();
            shape.Set(new Vector2(0, -100), new Vector2(0, bbheight));
            wall.CreateFixture(shape, 0.0f);

            wall = mPhysicsWorld.CreateBody(new BodyDef());
            shape = new EdgeShape();
            shape.Set(new Vector2(bbwidth, -100), new Vector2(bbwidth, bbheight));
            wall.CreateFixture(shape, 0.0f);

            // add a circle
            mPhysicsBall = AddCircle(new Vector2(10, 10), 20);

            // add a player
            mPhysicsPlayer = AddCircle(new Vector2(100, 100), 95, BodyType.Kinematic);
        }

        private Body AddCircle(Vector2 position, int radius, BodyType bt = BodyType.Dynamic)
        {
            Shape circle = new CircleShape();
            circle._radius = PixelsToMeters(radius);

            FixtureDef fd = new FixtureDef();
            fd.shape = circle;
            fd.restitution = 1.0f;
            fd.friction = 0;
            fd.density = 1.0f;

            BodyDef bd = new BodyDef();
            bd.type = bt;
            bd.position = PixelsToMeters(position);

            Body body = mPhysicsWorld.CreateBody(bd);
            body.CreateFixture(fd);

            return body;
        }


        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            mDefaultFont = this.Content.Load<SpriteFont>("DefaultFont");

            mBallTexture = Content.Load<Texture2D>("slimegameart-02");
            mPlayerTexture = Content.Load<Texture2D>("slimegameart-01");
        }

        protected override void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();
            if (ks.IsKeyDown(Keys.Escape))
            {
                this.Exit();
            }

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                this.Exit();
            }

            // update player
            if (mKinect.get_player(0) != null)
            {
                mPhysicsPlayer.SetLinearVelocity((PixelsToMeters(mKinect.get_player(0).get_kinect_face().mCenter) - mPhysicsPlayer.Position) / (float)gameTime.ElapsedGameTime.TotalSeconds);
            }

            mPhysicsWorld.Step((float)gameTime.ElapsedGameTime.TotalSeconds, 10, 3);
            eb_update();

            base.Update(gameTime);


        }

        public void eb_reset_game()
        {
        }

        public void eb_update()
        {

            bool everythingOK = true;
            if (mKinect.trackedSkeletons.Count > 0)
            {

                for (int i = 0; i < 1; i++)
                {
                    SkeletonFaceTracker e = mKinect.get_player(i);
                    if (e == null)
                    {
                        everythingOK = false;
                    }
                }
            }
            else everythingOK = false;
            if (!everythingOK)
            {
                ebHasFace = false;
                ebGameState = gameState.FINDING_FACES;
            }
            else
            {
                ebHasFace = true;
                ebGameState = gameState.PLAYING;
            }

            if (ebGameState == gameState.PLAYING) // Wait for stable expression
            {
                if (ebHitPoints == 0)
                    ebGameState = gameState.LOSE;
            }
            else if (ebGameState == gameState.LOSE) // Tell player what expression to make
            {
                eb_reset_game();
                //TODO tell the player they lost
                ebGameState = gameState.PLAYING;
            }

            // new ball
            if (mPhysicsBall.Position.Y > PixelsToMeters(400))
            {
                mPhysicsWorld.DestroyBody(mPhysicsBall);
                mPhysicsBall = AddCircle(new Vector2(200, 10), 10);
            }
        }

        public void eb_draw()
        {
            spriteBatch.Begin();

            spriteBatch.Draw(mBallTexture, MetersToPixels(mPhysicsBall.Position) - new Vector2(20, 20), null, Color.White,mPhysicsBall.Rotation, new Vector2(0, 0), 20 / (mBallTexture.Width / 2.0f), SpriteEffects.None, 0);

            spriteBatch.Draw(mPlayerTexture, MetersToPixels(mPhysicsPlayer.Position) - new Vector2(mPlayerTexture.Width / 2, mPlayerTexture.Height / 2), Color.White);
            
            spriteBatch.End();
        }

        protected Boolean mouthOpen(int playerIndex)
        {
            return ebHasFace && mKinect.get_player(playerIndex).get_kinect_face().get_smoothed_AUs()[1] > .3f;
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            int fontHeightIncrement = 30;
            int fontHeight = 10;
            spriteBatch.Begin();
            Texture2D t2d = mKinect.get_color_texture(graphics.GraphicsDevice);
            if (t2d != null)
            {
                spriteBatch.Draw(t2d, new Rectangle(0, 0, 640, 480), Color.White);
                spriteBatch.DrawString(mDefaultFont, "Tracking working", new Vector2(10, fontHeight), Color.Black);
                fontHeight += fontHeightIncrement;
                spriteBatch.DrawString(mDefaultFont, "Faces found: " + mKinect.trackedSkeletons.Count, new Vector2(10, fontHeight), Color.Black);
            }

            for (int i = 0; i < 2; i++) //for each player
            {
                if (mKinect.get_player(i) != null)
                {
                    VertexPositionColor[] vertices = mKinect.get_player(i).get_face_model();
                    KinectFace playerFace = mKinect.get_player(i).get_kinect_face();
                    if (vertices != null)
                    {
                        fontHeight += fontHeightIncrement;
                        string s = "";
                        foreach (float f in playerFace.get_smoothed_AUs())
                            s += " " + (f >= 0 ? "+" : "") + f.ToString("0.00");
                        spriteBatch.DrawString(mDefaultFont, "AU: " + s, new Vector2(10, fontHeight), Color.Black);
                        mBasicEffect.CurrentTechnique.Passes[0].Apply();
                        graphics.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineStrip, vertices, 0, vertices.Length / 2);
                    }
                    spriteBatch.DrawString(mDefaultFont, "P" + (i + 1) + "", playerFace.mCenter, Color.BlueViolet);
                }
            }

            //Draw debug emoticons
            if (mKinect.get_player(0) != null)
            {
                if (mouthOpen(0))
                {
                    spriteBatch.DrawString(mDefaultFont, "P1 -> :O", new Vector2(10, 400), Color.Black);
                }
                else
                {
                    spriteBatch.DrawString(mDefaultFont, "P1 -> :|", new Vector2(10, 400), Color.Black);
                }
            }

            if (mKinect.get_player(1) != null)
            {
                if (mouthOpen(1))
                {
                    spriteBatch.DrawString(mDefaultFont, ":O <- P2", new Vector2(590, 400), Color.Black);
                }
                else
                {
                    spriteBatch.DrawString(mDefaultFont, ":| <- P2", new Vector2(590, 400), Color.Black);
                }
            }

            spriteBatch.End();
            eb_draw();
            base.Draw(gameTime);
        }

        private void SensorChooserOnKinectChanged(object sender, KinectChangedEventArgs kinectChangedEventArgs)
        {
            KinectSensor oldSensor = kinectChangedEventArgs.OldSensor;
            KinectSensor newSensor = kinectChangedEventArgs.NewSensor;

            if (oldSensor != null)
            {
                oldSensor.AllFramesReady -= mKinect.KinectSensorOnAllFramesReady;
                oldSensor.ColorStream.Disable();
                oldSensor.DepthStream.Disable();
                oldSensor.DepthStream.Range = DepthRange.Default;
                oldSensor.SkeletonStream.Disable();
                oldSensor.SkeletonStream.EnableTrackingInNearRange = false;
                oldSensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Default;
            }

            if (newSensor != null)
            {
                try
                {
                    newSensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);
                    newSensor.DepthStream.Enable(DepthImageFormat.Resolution320x240Fps30);
                    try
                    {
                        // This will throw on non Kinect For Windows devices.
                        newSensor.DepthStream.Range = DepthRange.Near;
                        newSensor.SkeletonStream.EnableTrackingInNearRange = true;
                    }
                    catch (InvalidOperationException)
                    {
                        newSensor.DepthStream.Range = DepthRange.Default;
                        newSensor.SkeletonStream.EnableTrackingInNearRange = false;
                    }

                    newSensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;
                    newSensor.SkeletonStream.Enable();
                    newSensor.AllFramesReady += mKinect.KinectSensorOnAllFramesReady;
                    mKinect.set_kinect(newSensor);
                }
                catch (InvalidOperationException)
                { }
            }
        }
    }
}
