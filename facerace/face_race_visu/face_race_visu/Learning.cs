﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

//kinect stuff 
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;

//for serialization
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using Statistic;
namespace face_race_visu
{
	class Decider
	{
		public class Expression
		{
			public string mName;
			public MatrixLibrary.Matrix mCVM = new MatrixLibrary.Matrix(6, 6);
            public double[] mMean = new double[6];
			public double score(double[] testme)
			{
	            double[] ipme = new double[6];
	            for(int i = 0; i < 6; i++)
	                ipme[i] = mMean[i]-testme[i];
	            MatrixLibrary.Matrix tscore = MatrixLibrary.Matrix.Multiply(MatrixLibrary.Matrix.Inverse(mCVM),new MatrixLibrary.Matrix(MatrixLibrary.Matrix.OneD_2_TwoD(ipme)));
	            tscore = MatrixLibrary.Matrix.Multiply(MatrixLibrary.Matrix.Transpose(new MatrixLibrary.Matrix(MatrixLibrary.Matrix.OneD_2_TwoD(ipme))),tscore);
	            return tscore.in_Mat[0,0];
			}
		}
		string[] mFiles = {"smile.frexpr","frown.frexpr","mouth_open.frexpr"};
		Expression[] mExpressions;
		public Decider()
		{
            Stream stream = null;
			mExpressions = new Expression[mFiles.Length];
			for(int i = 0; i < mFiles.Length; i++)
			{
                //TODO try/except./finally this
				IFormatter formatter = new BinaryFormatter();
	            stream = new FileStream(mFiles[i], FileMode.Open, FileAccess.Read, FileShare.None);
                mExpressions[i] = new Expression();
				mExpressions[i].mName = Path.GetFileNameWithoutExtension(mFiles[i]);
	            mExpressions[i].mCVM.in_Mat = (double[,])formatter.Deserialize(stream);
                mExpressions[i].mMean = (double[])formatter.Deserialize(stream);
                stream.Close();
			}
		}
        public string classify(float[] testme)
		{
			double[] dbl = new double[6];
	        for(int i = 0; i < 6; i++)
                dbl[i] = (double)testme[i];
			string r = "";
			double minScore = (double)99999999;
			foreach(Expression e in mExpressions)
			{
			 	double score = e.score(dbl);
                //Debug.Print(score.ToString());
				if(score < minScore)
				{
					minScore = score;
					r = e.mName;
				}
			}
			return r;
		}
	}


    class Learning : Microsoft.Xna.Framework.Game
    {
        //"neutral.frexpr","smile.frexpr","frown.frexpr","mouth_open.frexpr
        string mFilename = "mouth_open.frexpr";
        List<float[]> mData = new List<float[]>();
        Statistics[] mStats = new Statistics[6];

        public double magnitude(double[] arr)
        {
            double r = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                r += arr[i] * arr[i];
            }
            return Math.Sqrt(r);
        }
        public float magnitude(float[] arr)
        {
            double r = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                r += arr[i] * arr[i];
            }
            return (float)Math.Sqrt(r);
        }
        public void write_to_file()
        {
            
            double[][] statData = new Double[6][];
            double[] mean = new Double[6];
            for (int i = 0; i < 6; i++)
            {
                statData[i] = new Double[mData.Count];
            }
            int count = 0;
            foreach (float[] e in mData)
            {
                if (e.Length != 6)
                    throw new Exception("data does not have 6 aus??");
                    
                for (int i = 0; i < 6; i++)
                {
                    statData[i][count] = e[i];
                }
                count++;
            }
            for (int i = 0; i < 6; i++)
            {
                mStats[i] = new Statistics(statData[i]);
                mean[i] = mStats[i].mean();
            }
            MatrixLibrary.Matrix cvm = Statistics.cov(mStats);
            MatrixLibrary.Matrix outputMatrix = MatrixLibrary.Matrix.Multiply(cvm,new MatrixLibrary.Matrix(MatrixLibrary.Matrix.OneD_2_TwoD(mean)));
            double[] output = MatrixLibrary.Matrix.TwoD_2_OneD(outputMatrix.in_Mat);

            Stream stream = null;
            try
            {
                IFormatter formatter = new BinaryFormatter();
                stream = new FileStream(mFilename, FileMode.Create, FileAccess.Write, FileShare.None);
                formatter.Serialize(stream, cvm.in_Mat);
                formatter.Serialize(stream, mean);

                /*System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(output.GetType());
                TextWriter textWriter = new StreamWriter(mFilename);
                serializer.Serialize(textWriter, output);
                textWriter.Close();*/
            }
            catch
            {
                //uhhh
            }
            finally
            {
                if (null != stream)
                    stream.Close();
            }
        }



        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont mDefaultFont;
        BasicEffect mBasicEffect;
        KinectData mKinect = new KinectData();
        public static SoundEffect mSampleSound;
        public static SoundEffect mSaveSound;
        bool mSpaceUp = true;
        bool mTUp = true;
        bool mPUp = true;
        //kinect crap
        private static readonly int Bgr32BytesPerPixel = (System.Windows.Media.PixelFormats.Bgr32.BitsPerPixel + 7) / 8;
        private readonly KinectSensorChooser sensorChooser = new KinectSensorChooser();

        public Learning()
        {
            graphics = new GraphicsDeviceManager(this);
            //graphics.ToggleFullScreen();
            Window.Title = "learning";
            graphics.PreferMultiSampling = true;
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();
            graphics.PreferredBackBufferHeight = 480;//GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            graphics.PreferredBackBufferWidth = 640;// GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            graphics.ApplyChanges();
            this.IsMouseVisible = true;
            Window.AllowUserResizing = true;

            //2d
            mBasicEffect = new BasicEffect(graphics.GraphicsDevice);
            mBasicEffect.VertexColorEnabled = true;
            mBasicEffect.Projection = Matrix.CreateOrthographicOffCenter
               (0, graphics.GraphicsDevice.Viewport.Width,     // left, right
                graphics.GraphicsDevice.Viewport.Height, 0,    // bottom, top
                0, 1);

            //kinect
            sensorChooser.KinectChanged += SensorChooserOnKinectChanged;
            sensorChooser.Start();

        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            mDefaultFont = this.Content.Load<SpriteFont>("DefaultFont");
            mSampleSound = this.Content.Load<SoundEffect>("crunch");
            mSaveSound = this.Content.Load<SoundEffect>("bbeep");
        }

        protected override void UnloadContent() { }

        protected override void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();
            
            if (ks.IsKeyDown(Keys.Escape))
                this.Exit();
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            if (ks.IsKeyDown(Keys.Space) && mSpaceUp)
            {
                if (mKinect.get_player(0) != null)
                {
                    KinectFace playerFace = mKinect.get_player(0).get_kinect_face();
                    if (playerFace.mIsTracked)
                    {
                        mData.Add(playerFace.get_smoothed_AUs());
                        mSampleSound.Play();
                    }
                }
                mSpaceUp = false;
            }
            if (ks.IsKeyDown(Keys.T) && mTUp)
            {
                mSaveSound.Play();
                write_to_file();
                mTUp = false;
				if (mKinect.get_player(0) != null)
                {
                    KinectFace playerFace = mKinect.get_player(0).get_kinect_face();
                    if (playerFace.mIsTracked)
                    {
                        float[] aus = playerFace.get_smoothed_AUs();
                        mSaveSound.Play();
                        double[] testme = new double[6];
                        for (int i = 0; i < 6; i++)
                        {
                            testme[i] = aus[i];
                        }

                        Stream stream = null;
                        try
                        {
                            MatrixLibrary.Matrix cvm = new MatrixLibrary.Matrix(6, 6);
                            double[] mean = new double[6];
                            IFormatter formatter = new BinaryFormatter();
                            stream = new FileStream(mFilename, FileMode.Open, FileAccess.Read, FileShare.None);
                            cvm.in_Mat = (double[,])formatter.Deserialize(stream);
                            mean = (double[])formatter.Deserialize(stream);


                            double[] ipme = new double[6];
                            for(int i = 0; i < 6; i++)
                                ipme[i] = mean[i]-testme[i];
                            MatrixLibrary.Matrix tscore = MatrixLibrary.Matrix.Multiply(MatrixLibrary.Matrix.Inverse(cvm),new MatrixLibrary.Matrix(MatrixLibrary.Matrix.OneD_2_TwoD(ipme)));
                            tscore = MatrixLibrary.Matrix.Multiply(MatrixLibrary.Matrix.Transpose(new MatrixLibrary.Matrix(MatrixLibrary.Matrix.OneD_2_TwoD(ipme))),tscore);
                            Debug.Print(tscore.ToString());
                        }
                        catch
                        {
                            //uhhh
                        }
                        finally
                        {
                            if (null != stream)
                                stream.Close();
                        }
                    }
                }
            }
            if (ks.IsKeyDown(Keys.P) && mPUp)
            {
                mPUp = false;
				if (mKinect.get_player(0) != null)
                {
                    KinectFace playerFace = mKinect.get_player(0).get_kinect_face();
                    if (playerFace.mIsTracked)
                    {
                        float[] aus = playerFace.get_smoothed_AUs();	
						Decider ded = new Decider();
						Debug.Print(ded.classify(aus));
					}
				}
            }

            if (ks.IsKeyUp(Keys.Space))
                mSpaceUp = true;
            if (ks.IsKeyUp(Keys.T))
                mTUp = true;
            if (ks.IsKeyUp(Keys.P))
                mPUp = true;
        }

       
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);
            int fontHeightIncrement = 30;
            int fontHeight = 10;
            spriteBatch.Begin();
            Texture2D t2d = mKinect.get_color_texture(graphics.GraphicsDevice);
            if (t2d != null)
            {
                spriteBatch.Draw(t2d, new Rectangle(0, 0, 640, 480), Color.White);
                spriteBatch.DrawString(mDefaultFont, "Tracking working", new Vector2(10, fontHeight), Color.Black);
                fontHeight += fontHeightIncrement;
                spriteBatch.DrawString(mDefaultFont, "Faces found: " + mKinect.trackedSkeletons.Count, new Vector2(10, fontHeight), Color.Black);
            }

            for (int i = 0; i < 2; i++) //for each player
            {
                if (mKinect.get_player(i) != null)
                {
                    VertexPositionColor[] vertices = mKinect.get_player(i).get_face_model();
                    KinectFace playerFace = mKinect.get_player(i).get_kinect_face();
                    if (vertices != null)
                    {
                        fontHeight += fontHeightIncrement;
                        string s = "";
                        foreach (float f in playerFace.get_smoothed_AUs())
                            s += " " + (f >= 0 ? "+" : "") + f.ToString("0.00");
                        spriteBatch.DrawString(mDefaultFont, "AU: " + s, new Vector2(10, fontHeight), Color.Black);
                        mBasicEffect.CurrentTechnique.Passes[0].Apply();
                        graphics.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineStrip, vertices, 0, vertices.Length / 2);
                    }
                    spriteBatch.DrawString(mDefaultFont, "P" + (i + 1) + "", playerFace.mCenter, Color.BlueViolet);
                }
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }


        public void render_message(SpriteBatch asb, SpriteFont asf, string amsg)
        {
            asb.DrawString(asf, amsg, new Vector2(100, 200), Color.Red);
        }

        private void SensorChooserOnKinectChanged(object sender, KinectChangedEventArgs kinectChangedEventArgs)
        {
            KinectSensor oldSensor = kinectChangedEventArgs.OldSensor;
            KinectSensor newSensor = kinectChangedEventArgs.NewSensor;

            if (oldSensor != null)
            {
                oldSensor.AllFramesReady -= mKinect.KinectSensorOnAllFramesReady;
                oldSensor.ColorStream.Disable();
                oldSensor.DepthStream.Disable();
                oldSensor.DepthStream.Range = DepthRange.Default;
                oldSensor.SkeletonStream.Disable();
                oldSensor.SkeletonStream.EnableTrackingInNearRange = false;
                oldSensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Default;
            }

            if (newSensor != null)
            {
                try
                {
                    newSensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);
                    newSensor.DepthStream.Enable(DepthImageFormat.Resolution320x240Fps30);
                    try
                    {
                        // This will throw on non Kinect For Windows devices.
                        newSensor.DepthStream.Range = DepthRange.Near;
                        newSensor.SkeletonStream.EnableTrackingInNearRange = true;
                    }
                    catch (InvalidOperationException)
                    {
                        newSensor.DepthStream.Range = DepthRange.Default;
                        newSensor.SkeletonStream.EnableTrackingInNearRange = false;
                    }

                    newSensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;
                    newSensor.SkeletonStream.Enable();
                    newSensor.AllFramesReady += mKinect.KinectSensorOnAllFramesReady;
                    mKinect.set_kinect(newSensor);
                }
                catch (InvalidOperationException)
                { }
            }
        }
    }
}
