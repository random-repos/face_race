﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Effect
{

    class EffectAnimatedGif : EffectBase
    {
        GifAnimation.GifAnimation mAnim;
        Vector2 mPosition = new Vector2();
        Vector2 mDim = new Vector2();
        Rectangle mRect = new Rectangle();
        int mFPS = 1;
        bool mDone = false;
        public Vector2 Dim
        {
            set
            {
                this.mDim = value;
                update_rectangle();
            }
            get
            {
                return this.mDim;
            }
        }
        public Vector2 Position
        {
            set
            {
                this.mPosition = value;
                update_rectangle();
            }
            get
            {
                return this.mPosition;
            }
        }
        public void update_rectangle()
        {
            mRect = new Rectangle((int)(mPosition.X - mDim.X / 2), (int)(mPosition.Y - mDim.Y / 2), (int)mDim.X, (int)mDim.Y);
        }

        public EffectAnimatedGif(GifAnimation.GifAnimation aAnim, Vector2 aPos, Vector2 aDim)
        {
            mAnim = aAnim;
            mPosition = aPos;
            mDim = aDim;
            //mAnim.Play();
            update_rectangle();
        }
        public override void update(float dt)
        {
            int before = mAnim.CurrentFrame;
            mAnim.Update((long)(dt * 10000000));
            if (mAnim.CurrentFrame < before)
                mDone = true;
            base.update(dt);
        }
        public override void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(mAnim.GetTexture(), mRect, Color.White);
        }
        public override bool is_finished()
        {
            return mDone;
        }
    }
}
