﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
namespace Effect
{

    class EffectManager
    {
        List<EffectBase> mEffects = new List<EffectBase>();
        public void update(float dt)
        {
            List<EffectBase> removal = new List<EffectBase>();
            foreach (EffectBase e in mEffects)
            {
                if (e.is_finished())
                    removal.Add(e);
                else
                    e.update(dt);
            }
            foreach (EffectBase e in removal)
                mEffects.Remove(e);

        }
        public void draw(SpriteBatch spriteBatch)
        {
            foreach (EffectBase e in mEffects)
                e.draw(spriteBatch);
        }
        public void add_effect(EffectBase aEffect)
        {
            mEffects.Add(aEffect);
        }
    }
}
