﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
namespace Effect
{
    class EffectExplodeParticle : EffectBase
    {

        public class Particle
        {
            public Vector2 mVelocity = new Vector2();
            public Vector2 mPosition = new Vector2();
            public float mRotation = 0;
            public float mAngularVelocity = 0;
            public float mTimer = 0;
            public float mExpire = 0;
        }

        List<Particle> mParticles = new List<Particle>();
        Texture2D mTex;
        Vector2 mGravity;
        float mExpire = 0;


        public Vector2 random_vector(Random rand)
        {
            return 2*(new Vector2((float)rand.NextDouble() - 0.5f, (float)rand.NextDouble() - 0.5f));
        }

        public EffectExplodeParticle(Texture2D aTex, Vector2 aPos, float aSize, float aTime, float aQuantity, float aRotation, Vector2 aGravity)
        {
            Random rand = new Random();
            for (int i = 0; i < aQuantity; i++)
            {
                Particle p = new Particle();
                p.mVelocity = aSize * random_vector(rand);
                p.mExpire = aTime;
                p.mRotation = (float)rand.NextDouble() * 6;
                p.mAngularVelocity = (float)(rand.NextDouble() - 0.5f) * aRotation;
                p.mPosition = aPos;
                mParticles.Add(p);
            }
            mGravity = aGravity;
            mTex = aTex;
            mExpire = aTime + 0;
        }
        public override void update(float dt)
        {
            foreach (Particle p in mParticles)
            {
                p.mTimer += dt;
                p.mRotation += p.mAngularVelocity * dt;
                p.mPosition += p.mVelocity * dt;
                p.mVelocity += mGravity * dt;
            }
            mExpire -= dt;
            base.update(dt);
        }
        public override void draw(SpriteBatch spriteBatch)
        {
            Rectangle rect = new Rectangle(0,0,mTex.Width,mTex.Height);
            Vector2 offset = new Vector2(mTex.Width/2,mTex.Height/2);
            foreach (Particle p in mParticles)
            {
                rect.X = (int)p.mPosition.X;
                rect.Y = (int)p.mPosition.Y;
                spriteBatch.Draw(mTex, rect, null, Color.White, p.mRotation, offset, SpriteEffects.None, 0);
            }
        }
        public override bool is_finished()
        {
            return mExpire <= 0;
        }
    }
}
