﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using Particles;

namespace Effect
{

    class EffectParticle : EffectBase
    {
        ParticleSystem mSystem;
        public EffectParticle(Vector2 aPos, Vector2 aTexture)
        {
            //mSystem.AddEmitter(new Vector2(0, 0), new Vector2(1, 0), new Vector2(0, 2 * (float)Math.PI), new Vector2(0, 0), new Vector2(1, 1), new Vector2(1, 1), new Color(Color.White), new Color(Color.White), new Color(Color.White), new Color(Color.White), new Vector2(1, 1), new Vector2(1, 1), 1000, aPos, aTexture);
        }
        public override void update(float dt)
        {
            base.update(dt);
        }
        public override void draw(SpriteBatch spriteBatch)
        {
            mSystem.Draw(spriteBatch, 1, new Vector2());
        }
        public override bool is_finished()
        {
            return (mSystem.EmitterList[0].number_alive() == 0);
        }
    }
}
