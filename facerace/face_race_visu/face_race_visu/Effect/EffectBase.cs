﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Effect
{
   
    class EffectBase
    {
        float mTime = 0;
        public virtual void update(float dt)
        {
            mTime += dt;
        }
        public virtual void draw(SpriteBatch spriteBatch)
        {
        }
        public virtual bool is_finished() 
        {
            return true; 
        }

    }

}
