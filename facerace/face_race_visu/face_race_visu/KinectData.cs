﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Graphics;

//kinect stuff 
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using Microsoft.Kinect.Toolkit.FaceTracking;

namespace face_race_visu
{
    class KinectData : KinectDataInterface
    {
        private KinectSensor mSensor;
        public void set_kinect(KinectSensor aSensor) { mSensor = aSensor; }

        private const uint MaxMissedFrames = 100;

        public byte[] colorImage;
        public ColorImageFormat colorImageFormat = ColorImageFormat.Undefined;
        public short[] depthImage;
        public DepthImageFormat depthImageFormat = DepthImageFormat.Undefined;

        public Skeleton[] skeletonData;
        public readonly Dictionary<int, SkeletonFaceTracker> trackedSkeletons = new Dictionary<int, SkeletonFaceTracker>();

        public int getNTrackedPlayers()
        {
            return trackedSkeletons.Count;
        }

        public SkeletonFaceTracker get_player(int aPlayerNumber)
        {
            return trackedSkeletons.Count > aPlayerNumber ? trackedSkeletons.ElementAt(aPlayerNumber).Value : null;

            //Dictionary<int, SkeletonFaceTracker>.Enumerator rator = trackedSkeletons.GetEnumerator();
            //for (int i = 0; i < aPlayerNumber; i++)
            //{
            //    rator.MoveNext();
            //}
            //return rator.Current.Value;
        }
        public Texture2D get_color_texture(GraphicsDevice aDevice)
        {
            if (colorImage != null)
            {
                Texture2D r;
                switch (colorImageFormat)
                {
                    case ColorImageFormat.RawYuvResolution640x480Fps15:
                    case ColorImageFormat.RgbResolution640x480Fps30:
                    case ColorImageFormat.YuvResolution640x480Fps15:
                        r = new Texture2D(aDevice, 640, 480, false, SurfaceFormat.Color);
                        break;
                    default:
                        throw new Exception("Kinect ColorImageFormat not handled");
                }
                byte[] newColorImage = new byte[colorImage.Length];
                for (int i = 0; i < colorImage.Length / 4; i++)
                {
                    Microsoft.Xna.Framework.Color c = new Microsoft.Xna.Framework.Color((int)colorImage[i * 4 + 2], (int)colorImage[i * 4 + 1], (int)colorImage[i * 4 + 0], 255);
                    newColorImage[i * 4 + 3] = c.A;
                    newColorImage[i * 4 + 0] = c.R;
                    newColorImage[i * 4 + 1] = c.G;
                    newColorImage[i * 4 + 2] = c.B;
                }
                r.SetData<byte>(newColorImage);
                return r;
            }
            return null;
        }
        public void KinectSensorOnAllFramesReady(object sender, AllFramesReadyEventArgs allFramesReadyEventArgs)
        {

            ColorImageFrame colorImageFrame = null;
            DepthImageFrame depthImageFrame = null;
            SkeletonFrame skeletonFrame = null;

            try
            {
                colorImageFrame = allFramesReadyEventArgs.OpenColorImageFrame();
                depthImageFrame = allFramesReadyEventArgs.OpenDepthImageFrame();
                skeletonFrame = allFramesReadyEventArgs.OpenSkeletonFrame();

                if (colorImageFrame == null || depthImageFrame == null || skeletonFrame == null)
                {
                    return;
                }

                // Check for image format changes.  The FaceTracker doesn't
                // deal with that so we need to reset.
                if (this.depthImageFormat != depthImageFrame.Format)
                {
                    this.ResetFaceTracking();
                    this.depthImage = null;
                    this.depthImageFormat = depthImageFrame.Format;
                }

                if (this.colorImageFormat != colorImageFrame.Format)
                {
                    this.ResetFaceTracking();
                    this.colorImage = null;
                    this.colorImageFormat = colorImageFrame.Format;
                }

                // Create any buffers to store copies of the data we work with
                if (this.depthImage == null)
                {
                    this.depthImage = new short[depthImageFrame.PixelDataLength];
                }

                if (this.colorImage == null)
                {
                    this.colorImage = new byte[colorImageFrame.PixelDataLength];
                }

                // Get the skeleton information
                if (this.skeletonData == null || this.skeletonData.Length != skeletonFrame.SkeletonArrayLength)
                {
                    this.skeletonData = new Skeleton[skeletonFrame.SkeletonArrayLength];
                }

                colorImageFrame.CopyPixelDataTo(this.colorImage);
                depthImageFrame.CopyPixelDataTo(this.depthImage);
                skeletonFrame.CopySkeletonDataTo(this.skeletonData);

                // Update the list of trackers and the trackers with the current frame information
                foreach (Skeleton skeleton in this.skeletonData)
                {
                    if (skeleton.TrackingState == SkeletonTrackingState.Tracked
                        || skeleton.TrackingState == SkeletonTrackingState.PositionOnly)
                    {
                        // We want keep a record of any skeleton, tracked or untracked.
                        if (!this.trackedSkeletons.ContainsKey(skeleton.TrackingId))
                        {
                            this.trackedSkeletons.Add(skeleton.TrackingId, new SkeletonFaceTracker(skeleton.TrackingId));
                        }

                        // Give each tracker the upated frame.
                        SkeletonFaceTracker skeletonFaceTracker;
                        if (this.trackedSkeletons.TryGetValue(skeleton.TrackingId, out skeletonFaceTracker))
                        {
                            skeletonFaceTracker.get_kinect_face().mIsTracked = true;
                            skeletonFaceTracker.OnFrameReady(this.mSensor, colorImageFormat, colorImage, depthImageFormat, depthImage, skeleton);
                            skeletonFaceTracker.LastTrackedFrame = skeletonFrame.FrameNumber;
                        }
                    }
                    else
                    {
                        SkeletonFaceTracker skeletonFaceTracker;
                        if (this.trackedSkeletons.TryGetValue(skeleton.TrackingId, out skeletonFaceTracker))
                        {
                            skeletonFaceTracker.get_kinect_face().mIsTracked = false;
                        }
                    }
                }

                this.RemoveOldTrackers(skeletonFrame.FrameNumber);
            }
            finally
            {
                if (colorImageFrame != null)
                {
                    colorImageFrame.Dispose();
                }

                if (depthImageFrame != null)
                {
                    depthImageFrame.Dispose();
                }

                if (skeletonFrame != null)
                {
                    skeletonFrame.Dispose();
                }
            }
        }

        private void RemoveOldTrackers(int currentFrameNumber)
        {
            var trackersToRemove = new List<int>();

            foreach (var tracker in this.trackedSkeletons)
            {
                uint missedFrames = (uint)currentFrameNumber - (uint)tracker.Value.LastTrackedFrame;
                if (missedFrames > MaxMissedFrames)
                {
                    // There have been too many frames since we last saw this skeleton
                    trackersToRemove.Add(tracker.Key);
                }
            }

            foreach (int trackingId in trackersToRemove)
            {
                this.RemoveTracker(trackingId);
            }
        }

        private void RemoveTracker(int trackingId)
        {
            this.trackedSkeletons[trackingId].Dispose();
            this.trackedSkeletons.Remove(trackingId);
        }

        private void ResetFaceTracking()
        {
            foreach (int trackingId in new List<int>(this.trackedSkeletons.Keys))
            {
                this.RemoveTracker(trackingId);
            }
        }
    }
}
