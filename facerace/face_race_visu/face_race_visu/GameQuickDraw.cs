/*
 *A 2 player quick draw game. A facial expression is displayed, and the player who makes the face first wins.
 *
 * 
 * TODO:
 * more graphics
 * sound
 * - wind howling during wait
 * - ricochet
 * - explosion
 * add rounds
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


//kinect stuff 
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;

namespace face_race_visu
{
    public class GameQuickDraw : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont mDefaultFont;
        BasicEffect mBasicEffect;
        //KinectDataInterface mKinect = new KinectDataSansKinect();
        KinectDataInterface mKinect = new KinectData();
        Dictionary<string, SoundEffect> sounds = new Dictionary<string,SoundEffect>();
        SoundEffectInstance soundQuietWind;
        SoundEffectInstance soundNoisyWind;


        //kinect crap
        private static readonly int Bgr32BytesPerPixel = (System.Windows.Media.PixelFormats.Bgr32.BitsPerPixel + 7) / 8;
        private readonly KinectSensorChooser sensorChooser = new KinectSensorChooser();
        Decider mDecider = new Decider();

        //GAME STATE
        enum gameState { FINDING_FACES, STATE_READYING_FACE, STATE_WAITING_FOR_INSTRUCTIONS, STATE_TIMING_FACE_CHANGE, STATE_REPORTING_RESULTS };
        enum gameResult { IN_PROGRESS, TOO_SLOW, TOO_EARLY, WIN};
        protected enum Expression { NEUTRAL, SMILE, MOUTH_OPEN }
        Expression targetExpression = Expression.NEUTRAL; //The expression players must make on the draw

        protected Expression getExpression(int playerIdx)
        {
            switch (mDecider.classify(mKinect.get_player(playerIdx).get_kinect_face().get_smoothed_AUs()))
            {
                case "frown":
                    return Expression.NEUTRAL;
                case "neutral":
                    return Expression.NEUTRAL;
                case "smile":
                    return Expression.SMILE;
                case "mouth_open":
                    return Expression.MOUTH_OPEN;
                default:
                    throw new Exception("not valid expression string");
            }
        }

        //TIMING CONSTANTS
        const int expressionHoldPhaseLength = 100;
        protected int preDrawPhaseLength = -1;  //time players wait before receiving instructions. not actually constant
        const int expressionWaitPhaseLength = 150;
        const int reportPhaseLength = 200;


        const int ebQueueLength = 2;
        gameState ebGameState = gameState.FINDING_FACES;
        int ebRemainingTicks = expressionHoldPhaseLength;
        bool ebHasFace = false;
        List<int> ebDrawSpeeds = null;
        List<gameResult> playerResults = null;
        GifAnimation.GifAnimation anim;
        class DrawOneExplosion
        {
            GifAnimation.GifAnimation mAnim;
            Vector2 mPos;
            public DrawOneExplosion(GifAnimation.GifAnimation aAnim, Vector2 aPos) { mAnim = aAnim; mPos = aPos; }
            public void update(long time) { mAnim.Update(time); }
            public void draw(SpriteBatch aBatch)
            {
                int tw = 400;
                int th = 400;
                aBatch.Draw(mAnim.GetTexture(), new Rectangle((int)(mPos.X - tw / 2), (int)(mPos.Y - th / 2), tw, th), Color.White);
            }
            public bool is_expired() { return false; }
        }
        DrawOneExplosion explosion = null;


        public GameQuickDraw()
        {
            graphics = new GraphicsDeviceManager(this);
            //graphics.ToggleFullScreen();
            Window.Title = "Face Race";
            graphics.PreferMultiSampling = true;
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();
            graphics.PreferredBackBufferHeight = 480;//GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            graphics.PreferredBackBufferWidth = 640;// GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            graphics.ApplyChanges();
            this.IsMouseVisible = true;
            Window.AllowUserResizing = true;

            //2d
            mBasicEffect = new BasicEffect(graphics.GraphicsDevice);
            mBasicEffect.VertexColorEnabled = true;
            mBasicEffect.Projection = Matrix.CreateOrthographicOffCenter
               (0, graphics.GraphicsDevice.Viewport.Width,     // left, right
                graphics.GraphicsDevice.Viewport.Height, 0,    // bottom, top
                0, 1);                   

            //kinect
            sensorChooser.KinectChanged += SensorChooserOnKinectChanged;
            sensorChooser.Start();

            //game logic etc...
            ebDrawSpeeds = new List<int>(2);
            ebDrawSpeeds.Insert(0, -1);
            ebDrawSpeeds.Insert(1, -1);

            playerResults = new List<gameResult>(2);
            playerResults.Insert(0, gameResult.IN_PROGRESS);
            playerResults.Insert(1, gameResult.IN_PROGRESS);

            this.soundNoisyWind.Play();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            mDefaultFont = this.Content.Load<SpriteFont>("DefaultFont");
            anim = Content.Load<GifAnimation.GifAnimation>("explosion1");

            //load sounds
            String[] soundFileNames = {
                                      "wind(calm)",
                                      "wind(fierce)",
                                      "explosion_01",
                                      "gunshot_01"
                                      };

            foreach (String s in soundFileNames)
            {
                this.sounds.Add(s, this.Content.Load<SoundEffect>(""+s));
            }

            this.soundQuietWind = sounds["wind(calm)"].CreateInstance();
            soundQuietWind.IsLooped = true;
            this.soundNoisyWind = sounds["wind(fierce)"].CreateInstance();
            this.soundNoisyWind.IsLooped = true;

        }

        protected override void UnloadContent(){}

        protected override void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();

            if (mKinect is KinectDataSansKinect)
            {
                ((KinectDataSansKinect) mKinect).handleKeyStrokes();
            }

            if (ks.IsKeyDown(Keys.Escape))
                this.Exit();
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            eb_update();

            base.Update(gameTime);

            if (explosion != null)
                explosion.update(gameTime.ElapsedGameTime.Ticks);
        }

        protected void clearResults()
        {
            playerResults[0] = gameResult.IN_PROGRESS;
            ebDrawSpeeds[0] = -1;

            playerResults[1] = gameResult.IN_PROGRESS;
            ebDrawSpeeds[1] = -1;
            
            Random r = new Random();
            int expInt = r.Next(3);

            //randomize
            targetExpression = expInt == 0 ? Expression.NEUTRAL : expInt == 1 ? Expression.SMILE : Expression.MOUTH_OPEN;

            preDrawPhaseLength = 50 + (int)Math.Pow(0.0 + r.Next(4), 2.0) * 30;
        }

        public void eb_update()
        {
            //if (mKinect.trackedSkeletons.Count > 0)
            if (mKinect.getNTrackedPlayers() == 2) // wait for exactly 2 faces to be on the screen
            {
                /*
                KinectData.SkeletonFaceTracker e = mKinect.trackedSkeletons.Values.First();
                if (e != null && e.mAUCoefficients != null)
                {
                    ebLowerLip.Enqueue(e.mAUCoefficients[1]);
                    if (ebLowerLip.Count > ebQueueLength)
                    {
                        ebLowerLip.Dequeue();
                    }
                    ebHasFace = true;
                }
                else
                {
                    ebHasFace = false;
                }
                */
                bool everythingOK = true;
                for (int i = 0; i < 2; i++)
                {
                    SkeletonFaceTracker e = mKinect.get_player(i);//mKinect.trackedSkeletons.Values.ElementAt(i);
                     if (e != null )
                     {
                        //ebFacialValues.Insert(i, e.get_kinect_face().get_smoothed_AUs());//mKinect.trackedSkeletons.Values.ElementAt(i).mAUCoefficients;
                     }
                     else
                     {
                         everythingOK = false;
                     }
                }
                ebHasFace = everythingOK;

                if (everythingOK && ebGameState == gameState.FINDING_FACES)
                {
                    ebGameState = gameState.STATE_READYING_FACE;
                    ebRemainingTicks = expressionHoldPhaseLength;
                }
            }
            else
            {
                ebHasFace = false;
                ebGameState = gameState.FINDING_FACES;
            }

            if (ebGameState == gameState.STATE_READYING_FACE) // Wait for stable expression
            {
                explosion = null; //reset explosion
                //Don't move on until stable expression
                //if (!mouthOpen(0) && !mouthOpen(1)) // TODO make players hold expression for a little bit
                if (getExpression(0) == Expression.NEUTRAL && getExpression(1) == Expression.NEUTRAL)
                {
                    ebRemainingTicks--; //force player to hold expression for a while

                    if (ebRemainingTicks <= 0) //Move on to Instruction Waiting phase!
                    {
                        clearResults();
                        ebGameState = gameState.STATE_WAITING_FOR_INSTRUCTIONS;
                        ebRemainingTicks = preDrawPhaseLength;
                    }

                }
                else
                {
                    ebRemainingTicks = expressionHoldPhaseLength;
                }
            }
            else
            {
                ebRemainingTicks--;
            }

            
            if (ebGameState == gameState.STATE_WAITING_FOR_INSTRUCTIONS) // Tell player what expression to make
            {
                //make player lose if they change expression prematurely
                
                if(ebRemainingTicks == 0)
                {
                    ebGameState = gameState.STATE_TIMING_FACE_CHANGE;
                    ebRemainingTicks = expressionWaitPhaseLength; 
                }

                for (int i = 0; i < 2; i++)
                {
                    //if (mouthOpen(i))
                    if(getExpression(i) != Expression.NEUTRAL)
                    {
                        this.sounds["gunshot_01"].Play();
                        playerResults[i] = gameResult.TOO_EARLY;
                        ebGameState = gameState.STATE_REPORTING_RESULTS;
                        explosion = new DrawOneExplosion(anim, mKinect.get_player(i).get_kinect_face().mCenter);
                    }
                }
            }
            else if (ebGameState == gameState.STATE_TIMING_FACE_CHANGE)
            {
                
                if (ebRemainingTicks == 0)
                {
                    // You were too slow!
                    for (int i = 0; i < 2; i++)
                    {
                        if (playerResults[i] != gameResult.WIN)
                        {
                            playerResults[i] = gameResult.TOO_SLOW;
                            ebDrawSpeeds[i] = -1;
                        }
                    }
                }

                bool noWinnerYet = playerResults[0] != gameResult.WIN && playerResults[1] != gameResult.WIN;

                for (int i = 0; i < 2; i++)
                {
                    if (noWinnerYet)
                    {
                        //if (mouthOpen(i))
                        if (getExpression(i) == this.targetExpression)
                        {
                            this.sounds["explosion_01"].Play();
                            explosion = new DrawOneExplosion(anim, mKinect.get_player((i + 1) % 2).get_kinect_face().mCenter);
                            playerResults[i] = gameResult.WIN;
                            ebDrawSpeeds[i] = expressionWaitPhaseLength - ebRemainingTicks;
                        }
                    }
                    else
                    {
                        if (getExpression(i) == this.targetExpression && playerResults[i] != gameResult.WIN)
                        {
                            playerResults[i] = gameResult.TOO_SLOW;
                            ebDrawSpeeds[i] = expressionWaitPhaseLength - ebRemainingTicks;
                        }
                    }

                }
                

                bool roundFinished = playerResults[0] != gameResult.IN_PROGRESS && playerResults[1] != gameResult.IN_PROGRESS;
                // decide whether player succeeded
                if (roundFinished)
                {
                    ebRemainingTicks = reportPhaseLength;
                    ebGameState = gameState.STATE_REPORTING_RESULTS;
                }
                
            }
            else if (ebGameState == gameState.STATE_REPORTING_RESULTS)
            {
                if (ebRemainingTicks == 0)
                {
                    ebGameState = gameState.STATE_READYING_FACE;
                    ebRemainingTicks = expressionHoldPhaseLength;
                }
            }


                //if (mouthOpen())
                //{
                //    ebSuccess = true;
                //}

        }

        protected Boolean mouthOpen(int playerIndex)
        {
            return ebHasFace && mKinect.get_player(playerIndex).get_kinect_face().get_smoothed_AUs()[1] > .5f;
        }

        public void eb_draw()
        {
            //EYEBROW GAME
            spriteBatch.Begin();
               if (ebGameState == gameState.FINDING_FACES)
                {
                    render_message(spriteBatch, mDefaultFont, "SEARCHING FOR FACES....");
                }

               if (ebHasFace)
               {

                   if (ebGameState == gameState.STATE_READYING_FACE)
                   {
                       render_message(spriteBatch, mDefaultFont, "GET READY & SHUT YO MOUTH!!!!!!!!!!!!!!!!!!!!!");
                   }
                   else if (ebGameState == gameState.STATE_WAITING_FOR_INSTRUCTIONS) // making the face!
                   {
                       render_message(spriteBatch, mDefaultFont, "WAIT FOR IT.....");
                   }
                   else if (ebGameState == gameState.STATE_TIMING_FACE_CHANGE)
                   {
                       render_message(spriteBatch, mDefaultFont, this.targetExpression.ToString());//"DRAW ! ! ! ! ! !");
                   }
                   else if (ebGameState == gameState.STATE_REPORTING_RESULTS)
                   {

                       if (playerResults[0] == gameResult.TOO_EARLY || playerResults[1] == gameResult.TOO_EARLY)
                       {
                           //TODO! Handle tie?
                           int gunJumper = playerResults[0] == gameResult.TOO_EARLY ? 0 : 1;
                           int winner = (gunJumper + 1) % 2;
                           render_message(spriteBatch, mDefaultFont, "Player " + (gunJumper + 1) + " jumped the gun! Player " + (winner + 1) + " wins the round!");
                       }
                       else if (playerResults[0] == gameResult.TOO_SLOW && playerResults[1] == gameResult.TOO_SLOW)
                       {
                           render_message(spriteBatch, mDefaultFont, "NO ONE DID ANYTHING!!!!!");
                       }
                       else if (playerResults[0] == gameResult.WIN && playerResults[1] == gameResult.WIN)
                       {
                           render_message(spriteBatch, mDefaultFont, "TIE!!");
                       }
                       else
                       {
                           int winner = playerResults[0] == gameResult.WIN ? 0 : 1;
                           int loser = (winner + 1) % 2;

                           if (ebDrawSpeeds[loser] != -1)
                           {
                               render_message(spriteBatch, mDefaultFont, "Player " + (winner + 1) + " wins the round by" + (ebDrawSpeeds[loser] - ebDrawSpeeds[winner]) + " frames!");
                           }
                           else
                           {
                               render_message(spriteBatch, mDefaultFont, "Player " + (winner + 1) + " wins, with a draw speed of" + (ebDrawSpeeds[winner]) + " frames!");
                           }
                       }


                       /*if (ebGameResult == gameResult.TOO_EARLY)
                       {
                           render_message(spriteBatch, mDefaultFont, "TOO EARLY!!!!!!!!!");
                       }
                       else if (ebGameResult == gameResult.TOO_SLOW)
                       {
                           render_message(spriteBatch, mDefaultFont, "TOO SLOW! YOU DED!");
                       }
                       else if (ebGameResult == gameResult.WIN)
                       {
                           render_message(spriteBatch, mDefaultFont, "YOU WIN! IT TOOK YOU " + ebDrawSpeed + " FRAMES TO OPEN YOUR MOUTH!");
                       }*/
                   }

                   //draw a progress bar for stuff
                   if (ebGameState != gameState.STATE_TIMING_FACE_CHANGE && ebGameState != gameState.STATE_WAITING_FOR_INSTRUCTIONS)
                   {
                       VertexPositionColor[] vertices = new VertexPositionColor[2];
                       vertices[0] = new VertexPositionColor(new Vector3(615, 465, 0), Color.HotPink);
                       vertices[1] = new VertexPositionColor(new Vector3(615 - (int)((600.0 * ebRemainingTicks) / expressionHoldPhaseLength), 465, 0), Color.HotPink);
                       mBasicEffect.CurrentTechnique.Passes[0].Apply();
                       graphics.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, vertices, 0, 1);
                   }

                   /*if (ebHasFace && ebLowerLip.Average() > .5f)
                   {
                       spriteBatch.DrawString(mDefaultFont, ":O", new Vector2(10, 400), Color.Black);
                   }
                   else
                   {
                       spriteBatch.DrawString(mDefaultFont, ":|", new Vector2(10, 400), Color.Black);
                   }*/
               }
               else
               {
               //    render_message(spriteBatch, mDefaultFont, "NO FACE");
               }
               if (explosion != null)
                   explosion.draw(spriteBatch);
            spriteBatch.End();
        }

        string to_emoticon(Expression aExp)
        {
            switch (aExp)
            {
                case Expression.MOUTH_OPEN:
                    return ":-O";
                case Expression.NEUTRAL:
                    return ":-|";
                case Expression.SMILE:
                    return ":-)";
                default:
                    return ":-?";
            }
        }
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            int fontHeightIncrement = 30;
            int fontHeight = 10;
            spriteBatch.Begin();
            Texture2D t2d = mKinect.get_color_texture(graphics.GraphicsDevice);
            if (t2d != null)
            {
                spriteBatch.Draw(t2d, new Rectangle(0, 0, 640, 480), Color.White);
                spriteBatch.DrawString(mDefaultFont, "Tracking working", new Vector2(10, fontHeight), Color.Black);
                fontHeight += fontHeightIncrement;
                spriteBatch.DrawString(mDefaultFont, "Faces found: " + mKinect.getNTrackedPlayers(), new Vector2(10, fontHeight), Color.Black);
            }
            
            
            /*foreach (KinectData.SkeletonFaceTracker e in mKinect.trackedSkeletons.Values)
            {
                //VertexPositionColor[] vertices = e.get_face_model();
                VertexPositionColor[] vertices = e.get_2d_face_model();
                if (vertices != null)
                {
                    fontHeight += fontHeightIncrement;
                    string s = "";
                    foreach (float f in e.get_kinect_face().get_smoothed_AUs())
                        s += " " + (f>=0?"+":"") + f.ToString("0.00");
                    spriteBatch.DrawString(mDefaultFont, "AU: " + s, new Vector2(10, fontHeight), Color.Black);
                    mBasicEffect.CurrentTechnique.Passes[0].Apply();
                    graphics.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineStrip, vertices, 0, vertices.Length/2);
                }
                spriteBatch.DrawString(mDefaultFont, "ID: " + e.get_kinect_face().mSkeletonTrackingId, e.get_kinect_face().mCenter, Color.BlueViolet);
            }*/

            for (int i = 0; i < 2; i++) //for each player
            {
                if (mKinect.get_player(i) != null)
                {
                    VertexPositionColor[] vertices = mKinect.get_player(i).get_face_model();
                    KinectFace playerFace = mKinect.get_player(i).get_kinect_face();
                    if (vertices != null)
                    {
                        fontHeight += fontHeightIncrement;
                        string s = "";
                        foreach (float f in playerFace.get_smoothed_AUs())
                            s += " " + (f >= 0 ? "+" : "") + f.ToString("0.00");
                        spriteBatch.DrawString(mDefaultFont, "AU: " + s, new Vector2(10, fontHeight), Color.Black);
                        mBasicEffect.CurrentTechnique.Passes[0].Apply();
                        graphics.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineStrip, vertices, 0, vertices.Length / 2);
                    }
                    spriteBatch.DrawString(mDefaultFont, "P" + (i+1) + "", playerFace.mCenter, Color.BlueViolet);
                }
            }

            //Draw debug emoticons
            /*
            if (mKinect.get_player(0) != null)
            {
                if (mouthOpen(0))
                {
                    spriteBatch.DrawString(mDefaultFont, "P1 -> :O", new Vector2(10, 400), Color.Black);
                }
                else
                {
                    spriteBatch.DrawString(mDefaultFont, "P1 -> :|", new Vector2(10, 400), Color.Black);
                }
            }

            if (mKinect.get_player(1) != null)
            {
                if (mouthOpen(1))
                {
                    spriteBatch.DrawString(mDefaultFont, ":O <- P2", new Vector2(590, 400), Color.Black);
                }
                else
                {
                    spriteBatch.DrawString(mDefaultFont, ":| <- P2", new Vector2(590, 400), Color.Black);
                }
            }*/

            
            if (mKinect.get_player(0) != null)
            {
                spriteBatch.DrawString(mDefaultFont, "P1 -> " + to_emoticon(getExpression(0)), new Vector2(10, 400), Color.Black);
            }

            if (mKinect.get_player(1) != null)
            {
                spriteBatch.DrawString(mDefaultFont, ":O <- P2" + to_emoticon(getExpression(1)), new Vector2(590, 400), Color.Black);
            }
            spriteBatch.End();
            eb_draw();
            base.Draw(gameTime);
        }


        public void render_message(SpriteBatch asb, SpriteFont asf, string amsg)
        {
            asb.DrawString(asf, amsg, new  Vector2(100,200),Color.Red);
        }

        private void SensorChooserOnKinectChanged(object sender, KinectChangedEventArgs kinectChangedEventArgs)
        {
            KinectSensor oldSensor = kinectChangedEventArgs.OldSensor;
            KinectSensor newSensor = kinectChangedEventArgs.NewSensor;

            if (oldSensor != null)
            {
                oldSensor.AllFramesReady -= mKinect.KinectSensorOnAllFramesReady;
                oldSensor.ColorStream.Disable();
                oldSensor.DepthStream.Disable();
                oldSensor.DepthStream.Range = DepthRange.Default;
                oldSensor.SkeletonStream.Disable();
                oldSensor.SkeletonStream.EnableTrackingInNearRange = false;
                oldSensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Default;
            }

            if (newSensor != null)
            {
                try
                {
                    newSensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);
                    newSensor.DepthStream.Enable(DepthImageFormat.Resolution320x240Fps30);
                    try
                    {
                        // This will throw on non Kinect For Windows devices.
                        newSensor.DepthStream.Range = DepthRange.Near;
                        newSensor.SkeletonStream.EnableTrackingInNearRange = true;
                    }
                    catch (InvalidOperationException)
                    {
                        newSensor.DepthStream.Range = DepthRange.Default;
                        newSensor.SkeletonStream.EnableTrackingInNearRange = false;
                    }

                    newSensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;
                    newSensor.SkeletonStream.Enable();
                    newSensor.AllFramesReady += mKinect.KinectSensorOnAllFramesReady;
                    mKinect.set_kinect(newSensor);
                }
                catch (InvalidOperationException)
                {}
            }
        }
    }
}
