﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace face_race_visu
{
    public interface SpriteGraphicsInterface 
    {
        void get_sprite_graphics(int index, out Rectangle crop, out Texture2D tex);
        int get_frame_count();
        int getWidth();
        int getHeight();
    }

    public class SpriteGraphicsImages : SpriteGraphicsInterface
    {
        List<Texture2D> mImages = new List<Texture2D>();
        public void add_image(Texture2D tex)
        {
            mImages.Add(tex);
        }
        public void get_sprite_graphics(int index, out Rectangle crop, out Texture2D tex)
        {
            if (index >= mImages.Count)
                throw new Exception("index out of range");
            tex = mImages[index];
            crop = new Rectangle(0, 0, tex.Width, tex.Height);
            
        }
        public int get_frame_count()
        {
            return mImages.Count;
        }

        public int getWidth()
        {
            return mImages[0].Width;
        }

        public int getHeight()
        {
            return mImages[0].Height;
        }
    }
}
