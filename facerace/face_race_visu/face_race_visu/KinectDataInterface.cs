﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Graphics;

//kinect stuff 
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using Microsoft.Kinect.Toolkit.FaceTracking;

namespace face_race_visu
{
    interface KinectDataInterface
    {
        void set_kinect(KinectSensor aSensor);


         SkeletonFaceTracker get_player(int aPlayerNumber);
         Texture2D get_color_texture(GraphicsDevice aDevice);
         void KinectSensorOnAllFramesReady(object sender, AllFramesReadyEventArgs allFramesReadyEventArgs);
         int getNTrackedPlayers();

    }
}
