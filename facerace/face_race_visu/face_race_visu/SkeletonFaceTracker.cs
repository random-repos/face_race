﻿using System;

using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using Microsoft.Kinect.Toolkit.FaceTracking;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace face_race_visu
{
    public class SkeletonFaceTracker : IDisposable
    {
        private KinectFace mKinectFace = new KinectFace();

        public KinectFace get_kinect_face() { return mKinectFace; }

        private static FaceTriangle[] faceTriangles;

        private EnumIndexableCollection<FeaturePoint, PointF> facePoints;

        private FaceTracker faceTracker;

        private bool lastFaceTrackSucceeded;

        private SkeletonTrackingState skeletonTrackingState;

        public float[] mAUCoefficients;

        public float[] mFaceLines;

        public int LastTrackedFrame { get; set; }


        public SkeletonFaceTracker(int aTrackingId) { mKinectFace.mSkeletonTrackingId = aTrackingId; }
        public void Dispose()
        {
            if (this.faceTracker != null)
            {
                this.faceTracker.Dispose();
                this.faceTracker = null;
            }
        }


        public VertexPositionColor[] get_2d_face_model()
        {
            if (!this.lastFaceTrackSucceeded || this.skeletonTrackingState != SkeletonTrackingState.Tracked)
                return null;

            VertexPositionColor[] r = new VertexPositionColor[mFaceLines.Length / 2];

            for (int i = 0; i < mFaceLines.Length / 2; i++)
            {
                r[i].Position = new Microsoft.Xna.Framework.Vector3((float)mFaceLines[2 * i], (float)mFaceLines[2 * i + 1], 0);
                r[i].Color = Microsoft.Xna.Framework.Color.Black;
            }
            return r;
        }

        public VertexPositionColor[] get_face_model()
        {
            if (!this.lastFaceTrackSucceeded || this.skeletonTrackingState != SkeletonTrackingState.Tracked)
                return null;

            var faceModelPts = new List<System.Windows.Point>();
            var faceModel = new List<FaceModelTriangle>();

            for (int i = 0; i < this.facePoints.Count; i++)
            {
                faceModelPts.Add(new System.Windows.Point(this.facePoints[i].X + 0.5f, this.facePoints[i].Y + 0.5f));
            }

            foreach (var t in faceTriangles)
            {
                var triangle = new FaceModelTriangle();
                triangle.P1 = faceModelPts[t.First];
                triangle.P2 = faceModelPts[t.Second];
                triangle.P3 = faceModelPts[t.Third];
                faceModel.Add(triangle);
            }



            VertexPositionColor[] r = new VertexPositionColor[faceModel.Count * 3];
            for (int i = 0; i < faceTriangles.Length; i++)
            {
                //r[3*i+0]=faceTriangles[i].First
            }

            for (int i = 0; i < faceModel.Count; i++)
            {
                r[3 * i + 0].Position = new Microsoft.Xna.Framework.Vector3((float)faceModel[i].P1.X, (float)faceModel[i].P1.Y, 0);
                r[3 * i + 0].Color = Microsoft.Xna.Framework.Color.Black;
                r[3 * i + 1].Position = new Microsoft.Xna.Framework.Vector3((float)faceModel[i].P2.X, (float)faceModel[i].P2.Y, 0);
                r[3 * i + 1].Color = Microsoft.Xna.Framework.Color.Black;
                r[3 * i + 2].Position = new Microsoft.Xna.Framework.Vector3((float)faceModel[i].P3.X, (float)faceModel[i].P3.Y, 0);
                r[3 * i + 2].Color = Microsoft.Xna.Framework.Color.Black;
            }
            return r;
        }

        /// <summary>
        /// Updates the face tracking information for this skeleton
        /// </summary>
        internal void OnFrameReady(KinectSensor kinectSensor, ColorImageFormat colorImageFormat, byte[] colorImage, DepthImageFormat depthImageFormat, short[] depthImage, Skeleton skeletonOfInterest)
        {
            this.skeletonTrackingState = skeletonOfInterest.TrackingState;

            if (this.skeletonTrackingState != SkeletonTrackingState.Tracked)
            {
                // nothing to do with an untracked skeleton.
                return;
            }

            while (this.faceTracker == null)
            {
                try
                {
                    this.faceTracker = new FaceTracker(kinectSensor);
                }
                catch (InvalidOperationException e)
                {
                    // During some shutdown scenarios the FaceTracker
                    // is unable to be instantiated.  Catch that exception
                    // and don't track a face.
                    System.Diagnostics.Debug.WriteLine("AllFramesReady - creating a new FaceTracker threw an InvalidOperationException");
                    this.faceTracker = null;
                }
            }

            if (this.faceTracker != null)
            {
                FaceTrackFrame frame = this.faceTracker.Track(
                    colorImageFormat, colorImage, depthImageFormat, depthImage, skeletonOfInterest);
                this.lastFaceTrackSucceeded = frame.TrackSuccessful;
                if (this.lastFaceTrackSucceeded)
                {
                    mKinectFace.add_AU_frames(frame.get_AUs());
                    PointF center = frame.get_center();
                    //if(!(center.X == 0  && center.Y !=0))
                    mKinectFace.mCenter = new Microsoft.Xna.Framework.Vector2(center.X, center.Y);

                    Vector3DF rotation = frame.Rotation;
                    mKinectFace.Rotation = new Microsoft.Xna.Framework.Vector3(rotation.X, rotation.Y, rotation.Z);
                    //mFaceLines = frame.get_2d_shape_points();
                    if (faceTriangles == null)
                    {
                        // only need to get this once.  It doesn't change.
                        faceTriangles = frame.GetTriangles();
                    }

                    this.facePoints = frame.GetProjected3DShape();
                }
            }
        }

        private struct FaceModelTriangle
        {
            public System.Windows.Point P1;
            public System.Windows.Point P2;
            public System.Windows.Point P3;
        }
    }
}