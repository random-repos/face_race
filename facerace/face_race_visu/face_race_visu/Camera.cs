﻿using System;
using Microsoft.Xna.Framework;

namespace face_race_visu
{

    public class Camera
    {
        protected Vector2 mPos;
        protected Vector2 mBasePos;
        protected Vector2 mScreenDim;
        float mShakeTimer = 0;
        public Vector2 Pos
        {
            set
            {
                this.mPos = value;
                this.mBasePos = value;
            }
            get
            {
                return this.mBasePos;
            }
        }


        public Camera(Vector2 aScreenDimensions)
        {
            mScreenDim = aScreenDimensions;
        }
        public void update(GameTime time)
        {
            mShakeTimer -= (float)time.ElapsedGameTime.TotalSeconds;
            if (mShakeTimer > 0)
            {
                Random rand = new Random();
                mPos = mBasePos + (new Vector2((float)rand.NextDouble() - 0.5f, (float)rand.NextDouble() - 0.5f)) * mShakeTimer * 40;
            }
        }
        public void shake()
        {
            mShakeTimer = 1;
        }
        //this is the window in GAME coordinates
        public Rectangle get_window_rectangle()
        {
            return new Rectangle((int)(-mScreenDim.X / 2), (int)(-mScreenDim.Y / 2), (int)(mScreenDim.X), (int)(mScreenDim.Y));
        }

        public int get_window_width()
        {
            return (int)(mScreenDim.X);
        }

        public int get_window_height()
        {
            return (int)(mScreenDim.Y);
        }

        public Rectangle transform_rectangle(Rectangle rect)
        {
            return new Rectangle(rect.X - (int)mPos.X + (int)mScreenDim.X / 2, rect.Y - (int)mPos.Y + (int)mScreenDim.Y / 2, rect.Width, rect.Height);
        }

        public Vector2 transform_vector(Vector2 vec)
        {
            return new Vector2(vec.X - mPos.X + mScreenDim.X / 2, vec.Y - mPos.Y + mScreenDim.Y / 2);
        }
    }
}
