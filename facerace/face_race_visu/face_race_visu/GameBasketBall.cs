﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


//kinect stuff 
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;


// Physics
using Box2D.XNA;

namespace face_race_visu
{
    public class GameBasketBall : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        static SpriteBatch spriteBatch;
        static SpriteFont mDefaultFont;

        Camera mCam = new Camera(new Vector2(640,480));//TODOD do not hardcode this

        // kinect stuff
        BasicEffect mBasicEffect;
        static KinectData mKinect = new KinectData();

        //kinect crap
        private static readonly int Bgr32BytesPerPixel = (System.Windows.Media.PixelFormats.Bgr32.BitsPerPixel + 7) / 8;
        private readonly KinectSensorChooser sensorChooser = new KinectSensorChooser();


        //eb
        enum gameState { FINDING_FACES, PLAYING, LOSE };
        gameState ebGameState = gameState.PLAYING;
        int ebHitPoints = 10;
        bool ebHasFace = false;
        Vector2 mFaceCenter = new Vector2(0, 0);
        Texture2D mBallTexture;
        Texture2D mPlayerTexture;
        Dictionary<String, Texture2D> mImages = new Dictionary<string, Texture2D>();

        World mPhysicsWorld;

        List<PhysicsSprite> mSprites = new List<PhysicsSprite>();

        PhysicsBallSprite mBallSprite;

        //TODO these physics sprites should likely be put in their own files

        class PhysicsSprite : Sprite
        {
            public static  World mPhysicsWorld;
            protected Body mBody;
            protected Texture2D mImg = null;

            public Body Body
            {
                get
                {
                    return mBody;
                }
            }

            public PhysicsSprite(Body body, Texture2D img = null) : base((body == null)?new Vector2(0,0):body.Position)
            {
                mBody = body;
                mImg = img;
            }

            public override void update(GameTime gameTime)
            {
                mPos = MetersToPixels(mBody.Position);
            }

            public override void draw(Camera cam = null)
            {
                if (cam == null)
                    draw(new Camera(new Vector2(640,480))); //TODO dont hardcaode this
                if (mImg != null)
                {
                    GameBasketBall.spriteBatch.Draw(mImg, MetersToPixels(mBody.Position) - new Vector2(mImg.Width / 2, mImg.Height / 2) - cam.Pos, Color.White);
                }
            }
        }

        class BoxSprite : PhysicsSprite
        {
            PolygonShape mShape;
            Rectangle mRect;
            public BoxSprite(Rectangle rect, Texture2D img = null) : base(null,img)
            {
                mRect = rect;
                mBody = (mPhysicsWorld.CreateBody(new BodyDef()));
                mShape = new PolygonShape();
                mShape.SetAsBox(PixelsToMeters(rect.Width), PixelsToMeters(rect.Height), PixelsToMeters(new Vector2(rect.Center.X, rect.Center.Y)), 0);
                mBody.CreateFixture(mShape,0.0f);
                mPos = new Vector2(0, 0);
            }
            public override void draw(Camera cam = null)
            {
                if (cam == null)
                    draw(new Camera(new Vector2(640, 480)));//TODO do not hardcode
                if (mImg != null)
                {
                    //Vector2 center = new Vector2(mRect.Center.X, mRect.Center.Y);
                    Rectangle other_rect = new Rectangle(mRect.X,mRect.Y,mRect.Width,mRect.Height);
                    Vector2 newCenter = -mPos - cam.Pos;
                    other_rect.Offset((int)newCenter.X,(int)newCenter.Y);
                    GameBasketBall.spriteBatch.Draw(mImg, other_rect, Color.White);
                    //GameBasketBall.spriteBatch.Draw(mImg, MetersToPixels(mBody.Position) - new Vector2(mImg.Width / 2, mImg.Height / 2) - cam.Pos, Color.White);
                }
            }
        };

        class PhysicsBallSprite : PhysicsSprite
        {

            public PhysicsBallSprite(Body body, Texture2D img = null) : base(body, img)
            {
            }

            public override void update(GameTime gameTime)
            {
                // new ball
                if (this.mBody.Position.Y > PixelsToMeters(400))
                {
                    mPhysicsWorld.DestroyBody(this.mBody);
                    this.mBody = AddCircle(new Vector2(200, 10), 10, mPhysicsWorld);
                }
                base.update(gameTime);
            }
        }

        class PhysicsPlayerSprite : PhysicsSprite
        {
            Vector2 mTarget;
            public PhysicsPlayerSprite(Body body, Texture2D img = null) : base(body, img)
            {
                mTarget = mPos;
            }

            public override void update(GameTime gameTime)
            {

                
                if (GameBasketBall.mKinect.get_player(0) != null)
                {
                    mTarget = mKinect.get_player(0).get_kinect_face().mCenter;
                }
                Vector2 difference = mTarget - mPos;
                float travel = MathHelper.Clamp(difference.Length(), 0, 10);
                Vector2 newPos = mPos;
                if (travel > 0)
                {
                    difference.Normalize();
                    newPos += difference * travel;
                }
                mBody.SetLinearVelocity((PixelsToMeters(newPos) - mBody.Position) / (float)gameTime.ElapsedGameTime.TotalSeconds);
                base.update(gameTime);
            }

            public override void draw(Camera cam = null)
            {
                base.draw(cam);

                spriteBatch.DrawString(GameBasketBall.mDefaultFont, "T", mTarget, Color.Red);
            }
        }

        const float pixelsPerMeter = 50;

        static float PixelsToMeters(int pixels)
        {
            return (float)pixels / pixelsPerMeter;
        }

        static Vector2 PixelsToMeters(Vector2 pixels)
        {
            return new Vector2(PixelsToMeters((int)pixels.X), PixelsToMeters((int)pixels.Y));
        }

        static int MetersToPixels(float meters)
        {
            return (int)(meters * pixelsPerMeter);
        }

        static Vector2 MetersToPixels(Vector2 meters)
        {
            return new Vector2(MetersToPixels(meters.X), MetersToPixels(meters.Y));
        }

        public GameBasketBall()
        {
            graphics = new GraphicsDeviceManager(this);

            Window.Title = "Goo Volleyball";
            graphics.PreferMultiSampling = true;
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();



            graphics.PreferredBackBufferHeight = 480;
            graphics.PreferredBackBufferWidth = 640;
            graphics.ApplyChanges();
            this.IsMouseVisible = true;
            Window.AllowUserResizing = true;

            //2d
            mBasicEffect = new BasicEffect(graphics.GraphicsDevice);
            mBasicEffect.VertexColorEnabled = true;
            mBasicEffect.Projection = Matrix.CreateOrthographicOffCenter
               (0, graphics.GraphicsDevice.Viewport.Width,     // left, right
                graphics.GraphicsDevice.Viewport.Height, 0,    // bottom, top
                0, 1);

            //kinect
            sensorChooser.KinectChanged += SensorChooserOnKinectChanged;
            sensorChooser.Start();

            mFaceCenter = new Vector2(graphics.PreferredBackBufferWidth / 5, graphics.PreferredBackBufferHeight / 2);
            
            mPhysicsWorld = new World(new Vector2(0, 9.8f), true);
            //Initialize the physics sprites
            PhysicsSprite.mPhysicsWorld = this.mPhysicsWorld;

            // add walls
            float bbwidth = PixelsToMeters(GraphicsDevice.PresentationParameters.BackBufferWidth);
            float bbheight = PixelsToMeters(GraphicsDevice.PresentationParameters.BackBufferHeight);

            //create some box walls
            
            BoxSprite leftWall = new BoxSprite(new Rectangle(-10, -3000, 20, 5000), mBallTexture);
            BoxSprite rightWall = new BoxSprite(new Rectangle((int)(GraphicsDevice.PresentationParameters.BackBufferWidth - 10), -3000, 20, 5000), mBallTexture);
            mSprites.Add(leftWall);
            mSprites.Add(rightWall);

            BoxSprite leftNet = new BoxSprite(new Rectangle(250, 0, 5, 75), mBallTexture);
            BoxSprite rightNet = new BoxSprite(new Rectangle(380, 0, 5, 75), mBallTexture);
            BoxSprite bottomNet = new BoxSprite(new Rectangle(255, 75, 75, 5), mBallTexture);
            mSprites.Add(leftNet);
            mSprites.Add(rightNet);
            mSprites.Add(bottomNet);
            
            
            /*
            Body wall = mPhysicsWorld.CreateBody(new BodyDef());
            EdgeShape shape = new EdgeShape();
            shape.Set(new Vector2(0, -100), new Vector2(0, bbheight));
            wall.CreateFixture(shape, 0.0f);

            wall = mPhysicsWorld.CreateBody(new BodyDef());
            shape = new EdgeShape();
            shape.Set(new Vector2(bbwidth, -100), new Vector2(bbwidth, bbheight));
            wall.CreateFixture(shape, 0.0f);
            */

            // add a circle
            mBallSprite = new PhysicsBallSprite(AddCircle(new Vector2(300, 10), 30, mPhysicsWorld), mBallTexture);
            mBallSprite.Body.SetLinearDamping(2.0f);
            mSprites.Add(mBallSprite);

            // add a player
            mSprites.Add(new PhysicsPlayerSprite(AddCircle(new Vector2(100, 100), 95, mPhysicsWorld, BodyType.Kinematic), this.mPlayerTexture));
        }

        //TODO this could be migrated into physics sprite
        private static Body AddCircle(Vector2 position, int radius, World physicsWorld, BodyType bt = BodyType.Dynamic)
        {
            Shape circle = new CircleShape();
            circle._radius = PixelsToMeters(radius);

            FixtureDef fd = new FixtureDef();
            fd.shape = circle;
            fd.restitution = 0.9f;
            fd.friction = 0;
            fd.density = 1.0f;

            BodyDef bd = new BodyDef();
            bd.type = bt;
            bd.position = PixelsToMeters(position);

            Body body = physicsWorld.CreateBody(bd);
            body.CreateFixture(fd);

            return body;
        }


        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            mDefaultFont = this.Content.Load<SpriteFont>("DefaultFont");

            mBallTexture = Content.Load<Texture2D>("slimegameart-02");
            mPlayerTexture = Content.Load<Texture2D>("slimegameart-01");
            
            String folder = "GameBasketBallAssets";
            
            mImages.Add("bg", Content.Load<Texture2D>(folder+"/"+"bg"));
        }

        protected override void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();
            if (ks.IsKeyDown(Keys.Escape))
            {
                this.Exit();
            }

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                this.Exit();
            }

            foreach (PhysicsSprite ps in mSprites)
            {
                ps.update(gameTime);
            }

            //// update player
            //if (mKinect.get_player(0) != null)
            //{
            //    mPhysicsPlayer.SetLinearVelocity((PixelsToMeters(mKinect.get_player(0).get_kinect_face().mCenter) - mPhysicsPlayer.Position) / (float)gameTime.ElapsedGameTime.TotalSeconds);
            //}

            mPhysicsWorld.Step((float)gameTime.ElapsedGameTime.TotalSeconds, 10, 3);
            eb_update();

            base.Update(gameTime);


        }

        public void eb_reset_game()
        {
        }

        public void eb_update()
        {

            bool everythingOK = true;
            if (mKinect.trackedSkeletons.Count > 0)
            {

                for (int i = 0; i < 1; i++)
                {
                    SkeletonFaceTracker e = mKinect.get_player(i);
                    if (e == null)
                    {
                        everythingOK = false;
                    }
                }
            }
            else everythingOK = false;
            if (!everythingOK)
            {
                ebHasFace = false;
                ebGameState = gameState.FINDING_FACES;
            }
            else
            {
                ebHasFace = true;
                ebGameState = gameState.PLAYING;
            }

            if (ebGameState == gameState.PLAYING) // Wait for stable expression
            {
                if (ebHitPoints == 0)
                    ebGameState = gameState.LOSE;
            }
            else if (ebGameState == gameState.LOSE) // Tell player what expression to make
            {
                eb_reset_game();
                //TODO tell the player they lost
                ebGameState = gameState.PLAYING;
            }


            if (mBallSprite.mPos.Y < 0)
            {
                Vector2 pos = mCam.Pos;
                pos.Y = mBallSprite.mPos.Y;
                mCam.Pos = pos;
            }
        }

        public void eb_draw()
        {
            double bgs = 2.0; //background scale

            int bgW = (int)bgs * mImages["bg"].Width;
            int bgH = (int)bgs * mImages["bg"].Height;

            Rectangle bgDestRect = new Rectangle(
                (int)-mCam.Pos.X, (int)-mCam.Pos.Y - bgH + FaceRace.getWindowHeight(),
                bgW, bgH
                );

            spriteBatch.Draw(mImages["bg"], bgDestRect, new Color(1,1,1,0.5f));

            foreach (PhysicsSprite ps in mSprites)
            {
                ps.draw(mCam);
            }
        }

        protected Boolean mouthOpen(int playerIndex)
        {
            return ebHasFace && mKinect.get_player(playerIndex).get_kinect_face().get_smoothed_AUs()[1] > .3f;
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            int fontHeightIncrement = 30;
            int fontHeight = 10;
            spriteBatch.Begin();

            Texture2D t2d = mKinect.get_color_texture(graphics.GraphicsDevice);
            if (t2d != null)
            {
                spriteBatch.Draw(t2d, new Rectangle(0, 0, 640, 480), Color.White);
                spriteBatch.DrawString(mDefaultFont, "Tracking working", new Vector2(10, fontHeight), Color.Black);
                fontHeight += fontHeightIncrement;
                spriteBatch.DrawString(mDefaultFont, "Faces found: " + mKinect.trackedSkeletons.Count, new Vector2(10, fontHeight), Color.Black);
            }

            eb_draw();


            for (int i = 0; i < 2; i++) //for each player
            {
                if (mKinect.get_player(i) != null)
                {
                    VertexPositionColor[] vertices = mKinect.get_player(i).get_face_model();
                    KinectFace playerFace = mKinect.get_player(i).get_kinect_face();
                    if (vertices != null)
                    {
                        fontHeight += fontHeightIncrement;
                        string s = "";
                        foreach (float f in playerFace.get_smoothed_AUs())
                            s += " " + (f >= 0 ? "+" : "") + f.ToString("0.00");
                        spriteBatch.DrawString(mDefaultFont, "AU: " + s, new Vector2(10, fontHeight), Color.Black);
                        mBasicEffect.CurrentTechnique.Passes[0].Apply();
                        graphics.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineStrip, vertices, 0, vertices.Length / 2);
                    }
                    spriteBatch.DrawString(mDefaultFont, "P" + (i + 1) + "", playerFace.mCenter, Color.YellowGreen);
                } 
            }

            //Draw debug emoticons
            if (mKinect.get_player(0) != null)
            {
                if (mouthOpen(0))
                {
                    spriteBatch.DrawString(mDefaultFont, "P1 -> :O", new Vector2(10, 400), Color.Black);
                }
                else
                {
                    spriteBatch.DrawString(mDefaultFont, "P1 -> :|", new Vector2(10, 400), Color.Black);
                }
            }

            if (mKinect.get_player(1) != null)
            {
                if (mouthOpen(1))
                {
                    spriteBatch.DrawString(mDefaultFont, ":O <- P2", new Vector2(590, 400), Color.Black);
                }
                else
                {
                    spriteBatch.DrawString(mDefaultFont, ":| <- P2", new Vector2(590, 400), Color.Black);
                }
            }

            spriteBatch.End();
            
            base.Draw(gameTime);
        }

        private void SensorChooserOnKinectChanged(object sender, KinectChangedEventArgs kinectChangedEventArgs)
        {
            KinectSensor oldSensor = kinectChangedEventArgs.OldSensor;
            KinectSensor newSensor = kinectChangedEventArgs.NewSensor;

            if (oldSensor != null)
            {
                oldSensor.AllFramesReady -= mKinect.KinectSensorOnAllFramesReady;
                oldSensor.ColorStream.Disable();
                oldSensor.DepthStream.Disable();
                oldSensor.DepthStream.Range = DepthRange.Default;
                oldSensor.SkeletonStream.Disable();
                oldSensor.SkeletonStream.EnableTrackingInNearRange = false;
                oldSensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Default;
            }

            if (newSensor != null)
            {
                try
                {
                    newSensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);
                    newSensor.DepthStream.Enable(DepthImageFormat.Resolution320x240Fps30);
                    try
                    {
                        // This will throw on non Kinect For Windows devices.
                        newSensor.DepthStream.Range = DepthRange.Near;
                        newSensor.SkeletonStream.EnableTrackingInNearRange = true;
                    }
                    catch (InvalidOperationException)
                    {
                        newSensor.DepthStream.Range = DepthRange.Default;
                        newSensor.SkeletonStream.EnableTrackingInNearRange = false;
                    }

                    newSensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;
                    newSensor.SkeletonStream.Enable();
                    newSensor.AllFramesReady += mKinect.KinectSensorOnAllFramesReady;
                    mKinect.set_kinect(newSensor);
                }
                catch (InvalidOperationException)
                { }
            }
        }
    }
}
