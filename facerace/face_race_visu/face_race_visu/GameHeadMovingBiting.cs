﻿/*
 * Game where player moves head, and closes mouth to control
 * avatar monster head which bites enemies
 */
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


//kinect stuff 
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;

using System.IO;

namespace face_race_visu
{
    public class GameHeadMovingBiting : Microsoft.Xna.Framework.Game
    {
        // //////////////////////////////////////////////////////////////////////////////////////////////////////
        // ENEMIES, SPRITES, ETC...
        // //////////////////////////////////////////////////////////////////////////////////////////////////////

        class HealthBar : AnimatedSprite
        {
            int mHealth = 0;

            public HealthBar(float fps, SpriteGraphicsInterface sprite) : base(new Vector2(0, 0), fps, sprite) { }

            public override void draw(Camera cam)
            {
                Rectangle window = cam.get_window_rectangle();
                mPos.X = window.X + 75;
                mPos.Y = window.Y + 75;

                for (int i = 0; i < mHealth; ++i)
                {
                    base.draw(cam);
                    mPos.X += mSprite.getWidth() + 10;
                }
            }

            public void setHealth(int health)
            {
                mHealth = health;
            }

            public void hit()
            {
                if (mHealth > 0)
                {
                    mHealth--;
                }
            }

            public bool isDead()
            {
                return mHealth == 0;
            }
        }

        class FakeBird : AnimatedSprite
        {
            GameHeadMovingBiting mGame;
            public bool mHasMouthBeenOpened = false;
            public FakeBird(SpriteGraphicsInterface aSprite, GameHeadMovingBiting game)
                : base(new Vector2(0, 0), 10, aSprite)
            {
                mGame = game;
                Random r = new Random();

                Camera aCamera = mGame.getCamera();
                mPos = aCamera.Pos;
            }

            public override void update(GameTime time)
            {
                base.update(time);
            }
            public override void draw(Camera cam)
            {
                if (mHasMouthBeenOpened)
                    base.draw(cam);
            }
        }

        class StupidBird : AnimatedSprite
        {
            int flightSpeed = 5;
            GameHeadMovingBiting mGame;

            public StupidBird(SpriteGraphicsInterface aSprite, GameHeadMovingBiting game)
                : base(new Vector2(0, 0), 10, aSprite)
            {
                mGame = game;
                Random r = new Random();

                Camera aCamera = mGame.getCamera();

                //prevent birds from spawing too close
                bool tooClose = true;
                for (int i = 0; i < 10 && tooClose; i++)
                {
                    mFlip = r.Next() % 2 == 0 ? true : false;

                    int winWidth = mGame.getCamera().get_window_rectangle().Width;
                    int winHeight = mGame.getCamera().get_window_rectangle().Height;

                    Rectangle window_rect = aCamera.get_window_rectangle();
                    if (mFlip)
                    {
                        mPos = new Vector2(-100 + -winWidth / 2, 0);
                        //(window_rect.X - 100, r.Next(window_rect.Y + 100, window_rect.Y + window_rect.Height - 100));
                    }
                    else
                    {
                        mPos = new Vector2(100 + winWidth / 2, 0);
                        //(window_rect.X + window_rect.Width + 100, r.Next(window_rect.Y + 100, window_rect.Y + window_rect.Height - 100));
                        flightSpeed *= -1;
                    }

                    mPos.Y = r.Next(-winHeight / 2 + 150, winHeight / 2 - 150);



                    //check to see if you're too close to the other sprites
                    tooClose = false;
                    foreach (Sprite s in mGame.getSprites())
                    {
                        if (!isMissile(s) && (mPos - s.mPos).Length() < 300)
                        {
                            tooClose = true;
                            break;
                        }
                    }

                }

            }



            public override void update(GameTime time)
            {
                mPos.X += (flightSpeed * mGame.gameSpeed);



                Rectangle biggerThanScreenRect = mGame.getCamera().get_window_rectangle();
                biggerThanScreenRect.Inflate(mGame.getCamera().get_window_rectangle().Width / 2, mGame.getCamera().get_window_rectangle().Height / 2);

                if (!biggerThanScreenRect.Contains(new Point((int)mPos.X, (int)mPos.Y)))
                {
                    dead = true;
                }
                base.update(time);
            }

        }

        class PlayerHead : Sprite
        {
            const double TIME_INVINCIBLE = 2;

            public Boolean mouthIsOpen = false;

            public Vector2 kinectMidpoint = new Vector2(640 / 2, 480 / 2);
            Vector2 moveScale = new Vector2(9f, 11f);//(5.0f, 6.0f);
            SpriteGraphicsInterface mSprite;

            float mOpenTime = 0;
            float mTimeHit = 999;
            float mEatTime = 999; //less than 0 means ate bird

            double mTimeInvincible = 0;  // player is invincible while greater than zero
            GameHeadMovingBiting mGame;

            public PlayerHead(SpriteGraphicsInterface aSprite, GameHeadMovingBiting game)
                : base(new Vector2(0, 0))
            {
                mSprite = aSprite;
                this.mGame = game;
            }

            public void update(GameTime time, KinectDataInterface kinect, List<Sprite> enemies)
            {
                mTimeInvincible -= time.ElapsedGameTime.TotalSeconds;
                mOpenTime += (float)time.ElapsedGameTime.TotalSeconds;
                mTimeHit += (float)time.ElapsedGameTime.TotalSeconds;
                mEatTime += (float)time.ElapsedGameTime.TotalSeconds;
                base.update(time);

                bool mouthWasOpen = mouthIsOpen;
                mouthIsOpen = kinect.get_player(0).get_kinect_face().get_smoothed_AUs()[1] > .15f; //mouth threshhold

                if (!mouthWasOpen && mouthIsOpen)
                    mOpenTime = 0;

                mPos = moveScale * (kinect.get_player(0).get_kinect_face().mCenter - kinectMidpoint);//+new Vector2(640, 480);

                mPos.Y = Math.Min(mGame.getCamera().get_window_height() / 2, Math.Max(-mGame.getCamera().get_window_height() / 2, mPos.Y));
                mPos.X = Math.Min(mGame.getCamera().get_window_width() / 2, Math.Max(-mGame.getCamera().get_window_width() / 2, mPos.X));


                bool bitBird = false;
                if (mouthWasOpen && !mouthIsOpen)
                {
                    mEatTime = 0;
                }

                foreach (Sprite e in enemies)
                {
                    if (mouthWasOpen && !mouthIsOpen) // just bit
                    {
                        if (e is StupidBird && (mPos - e.mPos).Length() < 250) // hit radius
                        {
                            bitBird = true;
                            //mBiteSomethingSound.Play();

                            /*
                             *             mImgs.Add("missile_ph", this.Content.Load<Texture2D>("missile_ph"));
        mSounds.Add("explosion_01", this.Content.Load<SoundEffect>("explosion_01"));
        mSounds.Add("Gah!", this.Content.Load<SoundEffect>("headmovingbiting/sfx/Gah!"));
        mSounds.Add("open_mouth", this.Content.Load<SoundEffect>("headmovingbiting/sfx/open_mouth"));
        mSounds.Add("close_mouth(no bird)", this.Content.Load<SoundEffect>("headmovingbiting/sfx/crush"));
        mSounds.Add("close_mouth(bird)", this.Content.Load<SoundEffect>("headmovingbiting/sfx/crush_bird"));
                             */
                            e.dead = true;
                            create_particle_explosion_A(e.mPos);

                            mEatTime = -0.25f;
                            this.mGame.score += Math.Max(1, (int) (this.mGame.comboLevel * this.mGame.gameSpeed));
                            this.mGame.comboLevel++;
                        }
                        else if (e is FakeBird && (mPos - e.mPos).Length() < 300)
                        {
                            //mBiteSomethingSound.Play();
                            e.dead = true;
                            create_particle_explosion_A(e.mPos);
                            mEatTime = -0.25f;

                            mGame.ebGameState = gameState.PLAYING;
                        }
                    }

                    //necessary for scripting the tutorial anncouncements
                    if (mouthIsOpen)
                        if (e is FakeBird)
                            ((FakeBird)e).mHasMouthBeenOpened = true;
                        else
                            if (e is FakeBird)
                                ((FakeBird)e).mHasMouthBeenOpened = false;


                    if (isMissile(e) && (mPos - e.mPos).Length() < 75)
                    {
                        mTimeHit = 0;
                        create_explosion(e.mPos);
                        e.dead = true;
                        //((HorizontalMissile)e).explode();
                        mSounds["explosion_01"].Play();
                        GameHeadMovingBiting.mMainCamera.shake();

                        if (!isInvincible())
                        {
                            mSounds["Gah!"].Play();
                            GameHeadMovingBiting.ebHitPoints.hit();
                            this.mGame.comboLevel = 0;
                            mTimeInvincible = TIME_INVINCIBLE;
                        }
                    }
                }


                if (mouthWasOpen && !mouthIsOpen) // just bit
                {

                    if (bitBird)
                    {
                        mSounds["close_mouth(bird)"].Play();
                        //mSounds["bird_death"].Play();
                    }
                    else
                    {
                        mSounds["close_mouth(no bird)"].Play();
                    }
                }


                if (!mouthWasOpen && mouthIsOpen) // just opened
                {
                    //mOpenMouthSound.Play();
                    mSounds["open_mouth"].Play();
                }
            }

            public override void draw(Camera cam)
            {
                if (GameHeadMovingBiting.spriteBatch != null)
                {

                    Vector2 loc = mPos;
                    /*
                    Vector2 dim = new Vector2(217 /2, 246 /2);
                    Rectangle rect = new Rectangle((int)(loc.X - dim.X / 2), (int)(loc.Y - dim.Y / 2), (int)(dim.X), (int)(dim.Y));
                    rect = cam.transform_rectangle(rect);
                    if (mouthIsOpen)
                    {
                        GameHeadMovingBiting.spriteBatch.Draw(mOpenTexture, rect, Color.White);
                    }
                    else
                    {
                        GameHeadMovingBiting.spriteBatch.Draw(mCloseTexture, rect, Color.White);
                    }*/

                    Rectangle cropRect;
                    Texture2D tex;

                    if (mTimeHit < 0.3f)
                    {
                        mSprite.get_sprite_graphics(3, out cropRect, out tex);
                    }
                    else if (mouthIsOpen)
                    {
                        if (mOpenTime < 0.2f)
                            mSprite.get_sprite_graphics(1, out cropRect, out tex);
                        else
                            mSprite.get_sprite_graphics(2, out cropRect, out tex);
                    }
                    else
                    {
                        if (mEatTime < 0)
                        {
                            mSprite.get_sprite_graphics(4, out cropRect, out tex);
                        }
                        else if (mEatTime < 0.25f)
                        {
                            mSprite.get_sprite_graphics(5, out cropRect, out tex);
                        }
                        else
                        {
                            mSprite.get_sprite_graphics(0, out cropRect, out tex);
                        }
                    }
                    //cropRect.X += (int)(loc.X - cropRect.Width / 2);
                    //cropRect.Y += (int)(loc.Y - cropRect.Height / 2);
                    //cropRect = cam.transform_rectangle(cropRect);

                    Rectangle drawRect = new Rectangle((int)(mPos.X), (int)(mPos.Y), cropRect.Width, cropRect.Height);

                    drawRect.Width = (int)(drawRect.Width * .65);
                    drawRect.Height = (int)(drawRect.Height * .65);

                    drawRect = cam.transform_rectangle(drawRect);


                    drawPlus(
                        cam.transform_vector(
                        new Vector2((int)(mPos.X), (int)(mPos.Y))
                        )
                        );

                    if (!isInvincible() || ((int)(this.mTimeInvincible * 20) % 2 == 0))
                    {
                        //GameHeadMovingBiting.spriteBatch.Draw(tex, cropRect, Color.White);

                        GameHeadMovingBiting.spriteBatch.Draw(tex, drawRect, cropRect,
                            //new Color(1f, 1f, 1f, .1f),
                        Color.White,
                        0, new Vector2(cropRect.Width / 2, cropRect.Height / 2),  //or should this be draw rect??
                        0,
                        0);

                    }

                    if (this.mGame.comboLevel > 0)
                    {

                        spriteBatch.DrawString(mGame.mDefaultFont, "COMBO\nx" + this.mGame.comboLevel + "",
                            cam.transform_vector(mPos + new Vector2(-250, 0)), // draw positin 
                            Color.White,
                            0, //rotation
                            new Vector2(0, 0), //origin
                            .35f, //scale
                            SpriteEffects.None,
                            0 //Sprite layer
                            );
                    }
                }


                //draw a plus over mpos

            }

            bool isInvincible()
            {
                return (mTimeInvincible > 0);
            }
        }

        class HorizontalMissile : AnimatedSprite
        {
            float speed = .1f;
            float maxSpeed = 12;
            float acceleration = .025f;
            bool facingRight;
            GameHeadMovingBiting mGame;
            Random r = new Random();


            SpriteGraphicsInterface mWarningGraphics;
            int mLastWarningState = 0;

            public HorizontalMissile(SpriteGraphicsInterface aSprite, SpriteGraphicsInterface aWarningGraphics, GameHeadMovingBiting game)
                : base(new Vector2(0, 0), 10, aSprite)
            {
                mGame = game;

                speed *= mGame.gameSpeed;
                maxSpeed *= mGame.gameSpeed;
                acceleration *= mGame.gameSpeed;

                mWarningGraphics = aWarningGraphics;

                Camera aCamera = mGame.getCamera();

                //prevent missiles from spawning too close
                bool tooClose = true;
                for (int i = 0; i < 150 && tooClose; i++)
                {
                    facingRight = r.Next(2) == 0 ? true : false;
                    mFlip = !facingRight;

                    Rectangle window_rect = aCamera.get_window_rectangle();
                    if (facingRight)
                    {
                        mPos = new Vector2(window_rect.X - 100, r.Next(window_rect.Y + 100, window_rect.Y + window_rect.Height - 100));
                    }
                    else
                    {
                        mPos = new Vector2(window_rect.X + window_rect.Width + 100, r.Next(window_rect.Y + 100, window_rect.Y + window_rect.Height - 100));
                    }




                    //check to see if you're too close to the other sprites
                    tooClose = false;
                    foreach (Sprite s in mGame.getSprites())
                    {
                        if ((mPos - s.mPos).Length() < 300)
                        {
                            tooClose = true;
                            break;
                        }
                    }

                }
            }
            public HorizontalMissile(SpriteGraphicsInterface aSprite, SpriteGraphicsInterface aWarningGraphics, GameHeadMovingBiting game, int yPos)
                : this(aSprite, aWarningGraphics, game)
            {
                this.mPos.Y = yPos;
            }

            //public void setPosition(Vector2 pos){ this.mPos = pos; }

            public void setFacingRight(bool facingRight)
            {
                this.facingRight = facingRight;
                this.mFlip = !facingRight;
            }

            public void setAcceleration(float acceleration)
            {
                this.acceleration = acceleration * mGame.gameSpeed;
            }

            public void setMaxSpeed(float maxSpeed)
            {
                this.maxSpeed = maxSpeed * mGame.gameSpeed;
            }

            public void setSpeed(float speed)
            {
                this.speed = speed * mGame.gameSpeed;
            }

            public override void update(GameTime time)
            {
                int motionFactor = facingRight ? 1 : -1;
                mPos.X += motionFactor * speed * mGame.gameSpeed;

                int margin = 300;

                //TODO choose bird death based on actual world coords
                /*if (
                    mPos.X < -mGame.getCamera().get_window_rectangle().Width
                    ||
                    mPos.X > mGame.getCamera().get_window_rectangle().Width
                    ||
                    mPos.Y < -mGame.getCamera().get_window_rectangle().Height
                    ||
                    mPos.Y > mGame.getCamera().get_window_rectangle().Height
                    )
                {
                    dead = true;
                }*/

                //TODO put this test in a function!
                //Check to see if the missile is way offscreen, and kill if so.
                Rectangle biggerThanScreenRect = mGame.getCamera().get_window_rectangle();
                biggerThanScreenRect.Inflate(mGame.getCamera().get_window_rectangle().Width / 2, mGame.getCamera().get_window_rectangle().Height / 2);

                if (!biggerThanScreenRect.Contains(new Point((int)mPos.X, (int)mPos.Y)))
                {
                    dead = true;
                }


                speed = (float)Math.Min(speed + acceleration, maxSpeed);

                base.update(time);
            }
            public Vector2 warning_location(Camera cam)
            {
                Rectangle rect = cam.get_window_rectangle();
                //intersect mPos + trajectory*t with rect
                //we can assume the missile is outside the screen
                Vector2 rel = mPos - new Vector2(rect.Center.X, rect.Center.Y);
                Vector2 position = Vector2.Zero;
                if (mPos.X < rect.X)
                    position.X = rect.X + 80;
                else if (mPos.X > rect.X + rect.Width)
                    position.X = rect.X + rect.Width - 80;
                return position;
            }
            public bool is_on_screen(Camera cam)
            {
                Rectangle rect = cam.get_window_rectangle();
                return rect.Contains(new Point((int)mPos.X, (int)mPos.Y));
            }
            public override void draw(Camera cam)
            {
                if (mSprite != null)
                {
                    Rectangle cropRect;
                    Texture2D tex;
                    mSprite.get_sprite_graphics(compute_index(), out cropRect, out tex);
                    Rectangle drawRect = new Rectangle((int)(mPos.X), (int)(mPos.Y), cropRect.Width, cropRect.Height);
                    drawRect.Width = drawRect.Width * 3 / 4;
                    drawRect.Height = drawRect.Height * 3 / 4;
                    drawRect = cam.transform_rectangle(drawRect);
                    GameHeadMovingBiting.spriteBatch.Draw(tex, drawRect, cropRect,
                        //new Color(1f, 1f, 1f, .1f),
                        Color.White,
                        0, new Vector2(cropRect.Width / 2, cropRect.Height / 2),
                        mFlip ? SpriteEffects.FlipHorizontally : 0,
                        0);
                    //draw a plus over mpos
                    drawPlus(cam.transform_vector(
                        new Vector2((int)(mPos.X), (int)(mPos.Y))
                        ));
                }


                if (!is_on_screen(cam))
                {
                    mLastWarningState += 1;
                    int index = (mLastWarningState / 2) % mWarningGraphics.get_frame_count();
                    Vector2 warningPos = warning_location(cam);
                    Rectangle cropRect;
                    Texture2D tex;
                    mWarningGraphics.get_sprite_graphics(index, out cropRect, out tex);
                    Rectangle drawRect = new Rectangle((int)(warningPos.X), (int)(warningPos.Y), cropRect.Width, cropRect.Height);
                    drawRect = cam.transform_rectangle(drawRect);
                    GameHeadMovingBiting.spriteBatch.Draw(tex, drawRect, cropRect,
                        //new Color(1f, 1f, 1f, .1f),
                        Color.White,
                        0, new Vector2(cropRect.Width / 2, cropRect.Height / 2),  //or should this be draw rect??
                        0,
                        0);
                }
            }
        }

        class DiagonalMissile : AnimatedSprite
        {
            float speed = 4f;
            Vector2 mTrajectory = new Vector2(0, 0);
            GameHeadMovingBiting mGame;

            int spawnDist = 100;
            SpriteGraphicsInterface mWarningGraphics;
            int mLastWarningState = 0;

            public DiagonalMissile(SpriteGraphicsInterface aSprite, SpriteGraphicsInterface aWarningGraphics, Vector2 target, GameHeadMovingBiting game)
                : base(new Vector2(0, 0), 10, aSprite)
            {
                this.mGame = game;
                mWarningGraphics = aWarningGraphics;

                //Pick a random location just outside the screen


                //Prevent missiles from spawning too close to one another
                bool tooClose = true;
                Random r = new Random();
                for (int i = 0; i < 50 && tooClose; i++)
                {
                    bool flip = r.Next() % 2 == 0 ? true : false;
                    bool willComeFromTopOrBottom = r.Next() % 2 == 0 ? true : false;

                    Rectangle window_rect = mGame.getCamera().get_window_rectangle();


                    if (willComeFromTopOrBottom)
                    {
                        mPos = new Vector2(r.Next(window_rect.X + 100, window_rect.X + window_rect.Width - spawnDist), 0);

                        if (flip)
                        {
                            mPos.Y = window_rect.Y - spawnDist;
                        }
                        else
                        {
                            mPos.Y = window_rect.Y + window_rect.Height + spawnDist;
                        }
                    }
                    else
                    {
                        mPos = new Vector2(0, r.Next(window_rect.Y + spawnDist, window_rect.Y + window_rect.Height - spawnDist));
                        if (flip)
                        {
                            mPos.X = window_rect.X - spawnDist;
                        }
                        else
                        {
                            mPos.X = window_rect.X + window_rect.Width + spawnDist;
                        }
                    }

                    //check to see if you're too close to the other sprites
                    tooClose = false;
                    foreach (Sprite s in mGame.getSprites())
                    {
                        if ((mPos - s.mPos).Length() < 300)
                        {
                            tooClose = true;
                            break;
                        }
                    }
                }


                //double degrees = Math.At//r.Next(360) / 180.0 * Math.PI;

                //Set the trajectory

                mTrajectory = target - mPos;//new Vector2((float)Math.Cos(degrees), (float)Math.Sin(degrees));
                mTrajectory.Normalize();

                //float windowDiagLength = new Vector2(game.getCamera().get_window_rectangle().Width, game.getCamera().get_window_rectangle().Height).Length();
                //Vector2 offset = game.getCamera().get_window_rectangle(). * offset;
                //mPos = target - windowDiagLength * trajectory;

            }

            public bool is_on_screen(Camera cam)
            {
                Rectangle rect = cam.get_window_rectangle();
                return rect.Contains(new Point((int)mPos.X, (int)mPos.Y));
            }
            public Vector2 warning_location(Camera cam)
            {
                Rectangle rect = cam.get_window_rectangle();
                //intersect mPos + trajectory*t with rect
                //we can assume the missile is outside the screen
                Vector2 rel = mPos - new Vector2(rect.Center.X, rect.Center.Y);
                Vector2 position = Vector2.Zero;
                float t;
                if (rel.Y > 0)
                {
                    t = ((rect.Y + rect.Height) - mPos.Y) / mTrajectory.Y;
                    position = mPos + mTrajectory * t;
                }
                else
                {
                    t = ((rect.Y) - mPos.Y) / mTrajectory.Y;
                    position = mPos + mTrajectory * t;
                }
                if (rel.X > 0)
                {
                    float t2 = ((rect.X + rect.Width) - mPos.X) / mTrajectory.X;
                    if (t2 > t)
                        position = mPos + mTrajectory * t2;
                }
                else
                {
                    float t2 = ((rect.X) - mPos.X) / mTrajectory.X;
                    if (t2 > t)
                        position = mPos + mTrajectory * t2;
                }
                Vector2 offset = new Vector2(mTrajectory.X, mTrajectory.Y);
                offset.Normalize();
                offset *= 80;
                return position + offset;
            }
            public override void update(GameTime time)
            {
                float windowDiagLength = new Vector2(mGame.getCamera().get_window_rectangle().Width, mGame.getCamera().get_window_rectangle().Height).Length();

                mTrajectory.Normalize();
                mPos = mPos + speed * mTrajectory * mGame.gameSpeed;

                Rectangle biggerThanScreenRect = mGame.getCamera().get_window_rectangle();
                biggerThanScreenRect.Inflate(2 * spawnDist, 2 * spawnDist);

                bool HeadedOffScreen = (mPos.X * mTrajectory.X) + (mPos.Y * mTrajectory.Y) > 0;

                if ((!biggerThanScreenRect.Contains(new Point((int)mPos.X, (int)mPos.Y)) || (mPos.Length() > windowDiagLength))
                    &&
                    HeadedOffScreen
                    )

                //(mPos.Length() > 2*windowDiagLength) // TODO decide if it's offscreen more intelligently
                {
                    dead = true;
                }
                base.update(time);
            }

            public Vector2 trajectory
            {
                get
                {
                    return mTrajectory;
                }

                set
                {
                    mTrajectory = value;
                    mTrajectory.Normalize();
                }
            }

            public void setPositionAndTarget(Vector2 pos, Vector2 target)
            {
                this.mPos = pos;
                mTrajectory = target - mPos;//new Vector2((float)Math.Cos(degrees), (float)Math.Sin(degrees));
                mTrajectory.Normalize();

            }

            public override void draw(Camera cam)
            {
                //if (cam == null)    draw(new Camera());
                if (mSprite != null)
                {
                    Rectangle cropRect;
                    Texture2D tex;
                    mSprite.get_sprite_graphics(compute_index(), out cropRect, out tex);

                    Rectangle drawRect = new Rectangle((int)(mPos.X), (int)(mPos.Y), cropRect.Width, cropRect.Height);

                    drawRect.Width = drawRect.Width / 2;
                    drawRect.Height = drawRect.Height / 2;

                    drawRect = cam.transform_rectangle(drawRect);


                    GameHeadMovingBiting.spriteBatch.Draw(tex, drawRect, cropRect,
                        //new Color(1f, 1f, 1f, .1f),
                        Color.White,
                        (float)Math.Atan2(mTrajectory.Y, mTrajectory.X), new Vector2(cropRect.Width / 2, cropRect.Height / 2), //or should this be draw rect??
                        0,//mFlip ? SpriteEffects.FlipHorizontally : 0, 
                        0);
                    //draw a plus over mpos
                    drawPlus(cam.transform_vector(
                        new Vector2((int)(mPos.X), (int)(mPos.Y))
                        ));
                }

                if (!is_on_screen(cam))
                {
                    mLastWarningState += 1;
                    int index = (mLastWarningState / 2) % mWarningGraphics.get_frame_count();
                    Vector2 warningPos = warning_location(cam);
                    Rectangle cropRect;
                    Texture2D tex;
                    mWarningGraphics.get_sprite_graphics(index, out cropRect, out tex);
                    Rectangle drawRect = new Rectangle((int)(warningPos.X), (int)(warningPos.Y), cropRect.Width, cropRect.Height);
                    drawRect = cam.transform_rectangle(drawRect);

                    GameHeadMovingBiting.spriteBatch.Draw(tex, drawRect, cropRect,
                        //new Color(1f, 1f, 1f, .1f),
                        Color.White,
                        0, new Vector2(cropRect.Width / 2, cropRect.Height / 2),  //or should this be draw rect??
                        0,
                        0);
                }


            }
        }

        // //////////////////////////////////////////////////////////////////////////////////////////////////////
        // WAVES
        // //////////////////////////////////////////////////////////////////////////////////////////////////////


        class Wave
        {
            protected GameHeadMovingBiting mGame;
            public Wave(GameHeadMovingBiting game)
            {
                this.mGame = game;
            }
            public virtual void update() { }
            public virtual bool isFinished() { return false; }
            public virtual Wave getNextWave() { return null; }


        }

        class Wave1 : Wave // Just horizontal missiles
        {
            long waveLength = 2000;
            int spawnDelay = 0;

            int startingMissileDensity = 4;
            int endingMisileDensity = 12;

            int startMissileDelay = 75;
            int endMissileDelay = 45;

            public int getMissileDensity()
            {
                double weight = waveLength / 2000.0;

                return (int)(mGame.gameSpeed * (weight * startingMissileDensity + (1 - weight) * endingMisileDensity));
            }

            public int getMissileDelay()
            {
                double weight = waveLength / 2000.0;

                return (int)(1.0 / mGame.gameSpeed * (weight * startMissileDelay + (1 - weight) * endMissileDelay));
            }

            public Wave1(GameHeadMovingBiting game)
                : base(game)
            {
            }

            public override void update()
            {
                int winH = this.mGame.getCamera().get_window_rectangle().Height;
                int winW = this.mGame.getCamera().get_window_rectangle().Width;

                if (waveLength > 0)
                {
                    List<Sprite> enemies = this.mGame.getSprites();

                    int nMissilesPresent = 0;
                    int nBirdsPresent = 0;
                    foreach (Sprite e in enemies)
                    {
                        if (e is StupidBird)
                        {
                            nBirdsPresent++;
                        }
                        if (GameHeadMovingBiting.isMissile(e))
                        {
                            nMissilesPresent++;
                        }
                    }

                    if (nBirdsPresent < 2)
                    {
                        this.mGame.addSprite(new StupidBird(mBirdGraphics, this.mGame));
                    }

                    if
                        (nMissilesPresent < getMissileDensity() && spawnDelay == 0)
                    //(nMissilesPresent < getMissileDensity() && waveLength % ((int)(60 / mGame.gameSpeed)) == 0)
                    {
                        spawnDelay = getMissileDelay();
                        for (int i = 0; i < (2)// && nMissilesPresent < getMissileDensity()
                            ; i++)
                        {
                            this.mGame.addSprite(new HorizontalMissile(mRocketGraphics, mWarningGraphics, this.mGame));
                            nMissilesPresent++;
                        }
                    }

                    // missile from left
                    /*if (waveLength % getMissileSpawnRate() == 0)
                    {
                        HorizontalMissile nuRocket = new HorizontalMissile(mRocketGraphics, mWarningGraphics, this.mGame);
                        nuRocket.mPos = new Vector2(- winW/2 - 100 , this.mGame.getPlayer().mPos.Y);
                        nuRocket.setFacingRight(true);
                        this.mGame.addSprite(nuRocket);

                        //this.mGame.addSprite(new HorizontalMissile(mRocketGraphics, mWarningGraphics, this.mGame));
                    }

                    if ((waveLength + getMissileSpawnRate()/2) % getMissileSpawnRate() == 0)
                    {
                        HorizontalMissile nuRocket = new HorizontalMissile(mRocketGraphics, mWarningGraphics, this.mGame);
                        nuRocket.mPos = new Vector2(winW / 2 + 100, this.mGame.getPlayer().mPos.Y);
                        nuRocket.setFacingRight(false);
                        this.mGame.addSprite(nuRocket);

                        //this.mGame.addSprite(new HorizontalMissile(mRocketGraphics, mWarningGraphics, this.mGame));
                    }*/


                    waveLength--;
                    spawnDelay = Math.Max(0, spawnDelay - 1);
                }
            }

            public override bool isFinished()
            {
                bool ret = waveLength <= 0;

                bool stillMissilesPresent = false;

                foreach (Sprite s in mGame.getSprites())
                {
                    if (isMissile(s))
                    {
                        stillMissilesPresent = true;
                    }
                }

                ret &= !stillMissilesPresent;

                if (ret)
                {
                    System.Diagnostics.Debug.Print("DONE!");
                }
                return ret;
            }
        }

        class HorizontalMissileSpam : Wave
        {
            const int WAVE_LENGTH = 1500;
            long ticksRemaining = WAVE_LENGTH;
            Random r = new Random();

            public HorizontalMissileSpam(GameHeadMovingBiting game)
                : base(game)
            {
            }

            public override void update()
            {
                if (ticksRemaining > 100)
                {
                    int winHt = this.mGame.getCamera().get_window_rectangle().Height;
                    int winWdt = this.mGame.getCamera().get_window_rectangle().Width;

                    if (ticksRemaining % ((int) (200 / mGame.gameSpeed)) == 0)
                    {
                        this.mGame.addSprite(new StupidBird(mBirdGraphics, this.mGame));
                    }

                    if (ticksRemaining % ((int) (100 / mGame.gameSpeed)) == 0) // distance between misile walls
                    {

                        // build a missile wall

                        float top = -this.mGame.getCamera().get_window_rectangle().Height * .5f;
                        float bottom = this.mGame.getCamera().get_window_rectangle().Height * .5f;
                        float verticalStep = 100;

                        int nMissilesPerWall = (int)Math.Ceiling((bottom - top) / verticalStep);
                        int gapWidth = 3;

                        //make a random gap in the wall
                        int gapStart = r.Next(nMissilesPerWall - gapWidth + 1);


                        float left = -200;//-this.mGame.getCamera().get_window_rectangle().Width * .35f;
                        float right = -100; //this.mGame.getCamera().get_window_rectangle().Width * .35f;
                        float horizontalStep = 99;


                        int missileIdx = 0;
                        for (float y = top; y <= bottom; y += verticalStep)
                        {
                            if (!(missileIdx >= gapStart && missileIdx < gapStart + gapWidth))
                            {

                                HorizontalMissile missile = new HorizontalMissile(mRocketGraphics, mWarningGraphics, this.mGame);
                                missile.mPos = new Vector2(-this.mGame.getCamera().get_window_rectangle().Width / 2 - 100, y);
                                missile.setFacingRight(true);

                                this.mGame.addSprite(missile);
                            }
                            missileIdx++;
                        }
                    }
                }
                ticksRemaining = Math.Max(ticksRemaining - 1, 0);

            }

            public override bool isFinished()
            {
                return ticksRemaining == 0;
            }
        }

        class Wave2 : Wave // Just horizontal missiles
        {
            long waveLength = 2000;
            int spawnDelay = 0;

            int startingMissileDensity = 12;
            int endingMisileDensity = 20;
            Random r = new Random();

            public int getMissileDensity()
            {
                double weight = waveLength / 2000.0;

                return (int)(mGame.gameSpeed * (weight * startingMissileDensity + (1 - weight) * endingMisileDensity));
            }

            public Wave2(GameHeadMovingBiting game)
                : base(game)
            {
            }

            public override void update()
            {
                int winH = this.mGame.getCamera().get_window_rectangle().Height;
                int winW = this.mGame.getCamera().get_window_rectangle().Width;

                if (waveLength > 0)
                {
                    List<Sprite> enemies = this.mGame.getSprites();

                    int nMissilesPresent = 0;
                    int nBirdsPresent = 0;
                    foreach (Sprite e in enemies)
                    {
                        if (e is StupidBird)
                        {
                            nBirdsPresent++;
                        }
                        if (GameHeadMovingBiting.isMissile(e))
                        {
                            nMissilesPresent++;
                        }
                    }

                    if (nBirdsPresent < 2)
                    {
                        this.mGame.addSprite(new StupidBird(mBirdGraphics, this.mGame));
                    }

                    if (nMissilesPresent < getMissileDensity() && waveLength % 75 == 0)
                    {
                        for (int i = 0; i < 4 && nMissilesPresent < getMissileDensity(); i++)
                        {

                            if (r.Next(2) == 0)
                            {
                                this.mGame.addSprite(new DiagonalMissile(mRocketGraphics, mWarningGraphics, mGame.getPlayer().mPos, this.mGame));
                            }
                            else
                            {
                                this.mGame.addSprite(new DiagonalMissile(mRocketGraphics, mWarningGraphics,
                                    new Vector2(
                                        (float)r.Next(-mGame.getCamera().get_window_width() / 4, r.Next(mGame.getCamera().get_window_width() / 4))
                                        ,
                                        (float)r.Next(-mGame.getCamera().get_window_height() / 4, r.Next(mGame.getCamera().get_window_height() / 4))
                                        ),
                                    this.mGame));
                            }

                            nMissilesPresent++;
                        }
                    }

                    // missile from left
                    /*if (waveLength % getMissileSpawnRate() == 0)
                    {
                        HorizontalMissile nuRocket = new HorizontalMissile(mRocketGraphics, mWarningGraphics, this.mGame);
                        nuRocket.mPos = new Vector2(- winW/2 - 100 , this.mGame.getPlayer().mPos.Y);
                        nuRocket.setFacingRight(true);
                        this.mGame.addSprite(nuRocket);

                        //this.mGame.addSprite(new HorizontalMissile(mRocketGraphics, mWarningGraphics, this.mGame));
                    }

                    if ((waveLength + getMissileSpawnRate()/2) % getMissileSpawnRate() == 0)
                    {
                        HorizontalMissile nuRocket = new HorizontalMissile(mRocketGraphics, mWarningGraphics, this.mGame);
                        nuRocket.mPos = new Vector2(winW / 2 + 100, this.mGame.getPlayer().mPos.Y);
                        nuRocket.setFacingRight(false);
                        this.mGame.addSprite(nuRocket);

                        //this.mGame.addSprite(new HorizontalMissile(mRocketGraphics, mWarningGraphics, this.mGame));
                    }*/


                    waveLength--;
                    spawnDelay = Math.Max(0, spawnDelay - 1);
                }
            }

            public override bool isFinished()
            {
                bool ret = waveLength <= 0;

                bool stillMissilesPresent = false;

                foreach (Sprite s in mGame.getSprites())
                {
                    if (isMissile(s))
                    {
                        stillMissilesPresent = true;
                    }
                }

                ret &= !stillMissilesPresent;

                if (ret)
                {
                    System.Diagnostics.Debug.Print("DONE!");
                }
                return ret;
            }
        }

        class CircleMissileSpam : Wave
        {
            long ticksRemaining = 1500;
            int spawnDelay = 0;
            Random r = new Random();

            public CircleMissileSpam(GameHeadMovingBiting game)
                : base(game)
            {
            }

            public override void update()
            {

                if (ticksRemaining > 0)
                {
                    List<Sprite> enemies = this.mGame.getSprites();

                    int nMissilesPresent = 0;
                    int nBirdsPresent = 0;
                    foreach (Sprite e in enemies)
                    {
                        if (e is StupidBird)
                        {
                            nBirdsPresent++;
                        }
                        if (GameHeadMovingBiting.isMissile(e))
                        {
                            nMissilesPresent++;
                        }
                    }

                    // Add some birds
                    if (nBirdsPresent < 2)
                    {
                        this.mGame.addSprite(new StupidBird(mBirdGraphics, this.mGame));
                    }

                    if (ticksRemaining % (int)(130 / mGame.gameSpeed) == 0)
                    {
                        int nMissiles = 35;
                        float circRadius = mGame.getCamera().get_window_width() / 2 + 100;

                        int gapWidth = 6;
                        int gapStart = r.Next(nMissiles - 3);

                        //double startAng = (1.0 * r.Next(nMissiles)) / nMissiles * 2 Math.PI;

                        int i = r.Next(nMissiles);
                        int missilesAdded = 0;

                        //for (int i = 0; i < nMissiles; i++)
                        while (missilesAdded < nMissiles - gapWidth) 
                        {
                            missilesAdded++;
                            //if (!(i >= gapStart && i < gapStart + gapWidth)) {
                                double angle = 1.0 * i / (nMissiles) * 2 * Math.PI;

                                

                                DiagonalMissile missile = new DiagonalMissile(mRocketGraphics, mWarningGraphics, new Vector2(0, 0), this.mGame);
                                Vector2 pos = new Vector2((float)Math.Cos(angle) * circRadius, (float)Math.Sin(angle) * circRadius);
                                missile.setPositionAndTarget(pos, new Vector2(0, 0));

                                //circRadius += 10;

                                //make missiles die when they reach then center of the screen
                                missile.addKillCondition(
                                    delegate(Sprite s)
                                    {
                                        return (s.mPos - new Vector2(0, 0)).Length() < 75;
                                    }
                                    );

                                this.mGame.addSprite(missile);
                            //}

                            i = (i + 1 % nMissiles);
                        }
                    }


                    ticksRemaining--;
                    spawnDelay = Math.Max(0, spawnDelay - 1);
                }
            }

            public override bool isFinished()
            {
                bool ret = ticksRemaining <= 0 && this.mGame.getSprites().Count() == 0;
                return ret;
            }
        }

        class SineMissileSpam : Wave
        {
            long ticksRemaining = 1500;
            int spawnDelay = 0;
            Random r = new Random();

            public SineMissileSpam(GameHeadMovingBiting game)
                : base(game)
            {
            }

            public override void update()
            {

                if (ticksRemaining > 0)
                {
                    List<Sprite> enemies = this.mGame.getSprites();

                    int nMissilesPresent = 0;
                    int nBirdsPresent = 0;
                    foreach (Sprite e in enemies)
                    {
                        if (e is StupidBird)
                        {
                            nBirdsPresent++;
                        }
                        if (GameHeadMovingBiting.isMissile(e))
                        {
                            nMissilesPresent++;
                        }
                    }

                    // Add some birds
                    if (nBirdsPresent < 2)
                    {
                        this.mGame.addSprite(new StupidBird(mBirdGraphics, this.mGame));
                    }

                    if (ticksRemaining % 20 == 0)
                    {
                        HorizontalMissile missileCeiling = new HorizontalMissile(mRocketGraphics, mWarningGraphics, this.mGame);
                        HorizontalMissile missileFloor = new HorizontalMissile(mRocketGraphics, mWarningGraphics, this.mGame);
                        missileCeiling.setFacingRight(false);
                        missileFloor.setFacingRight(false);

                        double sinFreq = 17;

                        double angle = ((ticksRemaining / 20) % sinFreq) / sinFreq * 2 * Math.PI;

                        Vector2 sinVal = new Vector2(
                           (float)(mGame.getCamera().get_window_width() / 2 + 100),
                           (float)(mGame.getCamera().get_window_height() / 3 * Math.Sin(angle))
                           );

                        missileCeiling.mPos = sinVal + new Vector2(0, -300);
                        missileFloor.mPos = sinVal + new Vector2(0, 300);

                        mGame.addSprite(missileCeiling);
                        mGame.addSprite(missileFloor);
                    }



                    ticksRemaining--;
                    spawnDelay = Math.Max(0, spawnDelay - 1);
                }
            }

            public override bool isFinished()
            {
                bool ret = ticksRemaining <= 0 && this.mGame.getSprites().Count() == 0;
                if (ret)
                {
                    System.Diagnostics.Debug.Print("DONE!");
                }
                return ret;
            }
        }

        class FinalWave : Wave
        {
            long waveLength = 0;
            int spawnDelay = 0;
            float maxNMissiles = 25;
            Random r = new Random();

            public FinalWave(GameHeadMovingBiting game)
                : base(game)
            {
            }

            public override void update()
            {
                if (waveLength <= 1500)
                {
                    List<Sprite> enemies = this.mGame.getSprites();

                    int nMissilesPresent = 0;
                    int nBirdsPresent = 0;
                    foreach (Sprite e in enemies)
                    {
                        if (e is StupidBird)
                        {
                            nBirdsPresent++;
                        }
                        if (GameHeadMovingBiting.isMissile(e))
                        {
                            nMissilesPresent++;
                        }
                    }

                    if (nBirdsPresent < 2)
                    {
                        this.mGame.addSprite(new StupidBird(mBirdGraphics, this.mGame));
                    }

                    if (nMissilesPresent < maxNMissiles)
                    {
                        DiagonalMissile newMissile = null;

                        switch (r.Next(2))
                        {
                            case 0:
                                newMissile = new DiagonalMissile(mRocketGraphics, mWarningGraphics, this.mGame.getPlayer().mPos, this.mGame);
                                break;

                            case 1:
                                newMissile = new DiagonalMissile(mRocketGraphics, mWarningGraphics, new Vector2(0, 0), this.mGame);
                                break;
                        }

                        for (int i = -4; i <= 4; i++)
                        {
                            if (i != 0)
                            {
                                DiagonalMissile leftMissile = new DiagonalMissile(mRocketGraphics, mWarningGraphics, new Vector2(0, 0), this.mGame);
                                leftMissile.mPos =
                                    newMissile.mPos
                                    +
                                    i * 50 * new Vector2(newMissile.trajectory.Y, -newMissile.trajectory.X)
                                    -
                                    Math.Abs(i) * 75 * newMissile.trajectory;
                                leftMissile.trajectory = newMissile.trajectory;
                                this.mGame.addSprite(leftMissile);
                            }
                        }

                        this.mGame.addSprite(newMissile);




                        /*
                        if (waveLength >= 750 && waveLength % 250 == 0) // throw in a missile spam to make it extra hard
                        {
                            int nMissiles = 20;
                            float circRadius = mGame.getCamera().get_window_width() / 2 + 100;

                            int gapWidth = 6;
                            int gapStart = r.Next(nMissiles - 3);

                            for (int i = 0; i < nMissiles; i++)
                            {
                                //if (!(i >= gapStart && i < gapStart + gapWidth))
                                //{
                                    double angle = 1.0 * i / (nMissiles) * 2 * Math.PI;

                                    DiagonalMissile missile = new DiagonalMissile(mRocketGraphics, mWarningGraphics, new Vector2(0, 0), this.mGame);
                                    Vector2 pos = new Vector2((float)Math.Cos(angle) * circRadius, (float)Math.Sin(angle) * circRadius);
                                    missile.setPositionAndTarget(pos, new Vector2(0, 0));

                                    //make missiles die when they reach then center of the screen
                                    missile.addKillCondition(
                                        delegate(Sprite s)
                                        {
                                            return (s.mPos - new Vector2(0, 0)).Length() < 75;
                                        }
                                        );

                                    this.mGame.addSprite(missile);
                                //}
                            }
                        }*/

                    }
                    maxNMissiles += .02f * mGame.gameSpeed; // gradually increase the max number of missiles
                    waveLength++;
                    spawnDelay = Math.Max(0, spawnDelay - 1);
                }

            }

            public override bool isFinished()
            {
                return waveLength > 1500 && this.mGame.getSprites().Count() == 0;
            }
        }

        /// //////////////////////////////////////////////////////////////////////////////////////////////////////
        // FIELDS 
        /// //////////////////////////////////////////////////////////////////////////////////////////////////////

        //Scoring stuff
        int mScore = 0;
        int mComboLevel = 0;

        bool mDisplayHeadRotationWarning = false;

        float mGameSpeed = 1.0f;
        public float gameSpeed
        {
            get
            {
                return mGameSpeed;
            }

            set
            {
                mGameSpeed = value;
            }
        }


        GraphicsDeviceManager graphics;
        public static SpriteBatch spriteBatch;
        SpriteFont mDefaultFont;
        BasicEffect mBasicEffect;
        KinectDataInterface mKinect = new KinectDataSansKinect();//new KinectData();

        public static Vector2 kinectOffset = new Vector2(0, 0);

        static Dictionary<string, Texture2D> mImgs = new Dictionary<string, Texture2D>();
        static Dictionary<String, SoundEffect> mSounds = new Dictionary<string, SoundEffect>();


        //kinect crap
        private static readonly int Bgr32BytesPerPixel = (System.Windows.Media.PixelFormats.Bgr32.BitsPerPixel + 7) / 8;
        private readonly KinectSensorChooser sensorChooser = new KinectSensorChooser();


        public enum gameState { INTRO, PLAYING, LOSE };
        public gameState ebGameState = gameState.PLAYING;

        const double TIME_GAME_OVER = 5;
        double mGameOverDelay = 0;

        int ebTicksElapsed = 0;
        static HealthBar ebHitPoints;
        //bool ebHasFace = false;
        List<Sprite> mEdibles = new List<Sprite>();
        public static Texture2D mOpenTexture;
        public static Texture2D mCloseTexture;
        public static Texture2D mBirdTexture;
        public static Texture2D mParticleATexture;
        //public static SoundEffect mOpenMouthSound;
        //public static SoundEffect mBiteSomethingSound

        Texture2D mPlayerTexture = null;

        PlayerHead mPlayerHead = null; //TODO fold player state into player class
        Queue<Wave> mWaves = new Queue<Wave>();

        static Effect.EffectManager mEffectManager = new Effect.EffectManager();
        static GifAnimation.GifAnimation mExplosionGif;

        //game core graphics
        static SpriteGraphicsImages mBirdGraphics = new SpriteGraphicsImages();
        static SpriteGraphicsImages mRocketGraphics = new SpriteGraphicsImages();
        static SpriteGraphicsImages mHeartGraphics = new SpriteGraphicsImages();
        static SpriteGraphicsImages mHeadGraphics = new SpriteGraphicsImages();
        static SpriteGraphicsImages mAnouncementGraphics = new SpriteGraphicsImages();
        static SpriteGraphicsImages mWarningGraphics = new SpriteGraphicsImages();

        static Camera mMainCamera;
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // END FIELDS
        // ///////////////////////////////////////////////////////////////////////////////////////////////////

        // //////////////////////////////////////////////////////////////////////////////////////////////////////
        // MEMBER FUNCTIONS
        // //////////////////////////////////////////////////////////////////////////////////////////////////////

        public GameHeadMovingBiting()
        {
            graphics = new GraphicsDeviceManager(this);

            //TODO restore
            //graphics.ToggleFullScreen();

            Window.Title = "+ + + + HMB_GAME + + + +";
            graphics.PreferMultiSampling = true;
            Content.RootDirectory = "Content";
        }

        //---------------
        //MEOW MEOW HIGH SCORE NONSENSE
        //---------------
        
        Texture2D mHighScorePic;
        public void get_score(out int score, out Texture2D tex)
        {
            score = 0;
            tex = null;
            FileStream stream = null;
            try
            {
                stream = File.Open("score.txt", FileMode.Open);
                if (stream.CanRead)
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        if (!reader.EndOfStream)
                        {
                            score = Convert.ToInt32(reader.ReadLine());
                        }
                    }
                }
            }
            catch
            {
                score = 0;
            }
            finally
            {
                if(stream != null)
                    stream.Close();
            }

            try
            {
                stream = File.Open("texture.png", FileMode.Open);
                if (stream.CanRead)
                    tex = Texture2D.FromStream(graphics.GraphicsDevice, stream);
            }
            catch
            {
                tex = null;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
        }

        public void write_score(int score)
        {
            
            FileStream stream = null;
            try
            {
                stream = File.Open("score.txt", FileMode.Create);
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    writer.WriteLine(score.ToString());
                }
            }
            catch
            {
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
        }

        public void snag_picture(bool save)
        {
            Vector2 imgCenter = this.mKinect.get_player(0).get_kinect_face().mCenter;
            int width = 100;
            int height = 100;
            Texture2D t2d = mKinect.get_color_texture(graphics.GraphicsDevice);
            RenderTarget2D render = new RenderTarget2D(graphics.GraphicsDevice,width,height);
            graphics.GraphicsDevice.SetRenderTarget(render);
            spriteBatch.Begin();
            spriteBatch.Draw(t2d,new Rectangle(0,0,width,height),new Rectangle((int)(imgCenter.X-width/2),(int)(imgCenter.Y-height/2),width,height),Color.White); 
            spriteBatch.End();
            graphics.GraphicsDevice.SetRenderTarget(null);
            mPlayerTexture = render;

            if (save)
            {
                FileStream stream = File.Open("texture.png", FileMode.Create);
                render.SaveAsPng(stream, width, height);
                stream.Close();
            }
        }


        public Camera getCamera()
        {
            return mMainCamera;
        }

        public void addSprite(Sprite e)
        {
            this.mEdibles.Add(e);
        }

        public static bool isMissile(Sprite e)
        {
            return e is HorizontalMissile || e is DiagonalMissile;
        }

        public List<Sprite> getSprites()
        {
            return this.mEdibles;
        }

        public Sprite getPlayer()
        {
            return this.mPlayerHead;
        }

        protected override void Initialize()
        {
            base.Initialize();
            graphics.PreferredBackBufferWidth = (int)get_window_dimensions().X;// GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            graphics.PreferredBackBufferHeight = (int)get_window_dimensions().Y;//GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            graphics.ApplyChanges();
            this.IsMouseVisible = true;
            Window.AllowUserResizing = true;

            //2d
            mBasicEffect = new BasicEffect(graphics.GraphicsDevice);
            mBasicEffect.VertexColorEnabled = true;
            mBasicEffect.Projection = Matrix.CreateOrthographicOffCenter
               (0, graphics.GraphicsDevice.Viewport.Width,     // left, right
                graphics.GraphicsDevice.Viewport.Height, 0,    // bottom, top
                0, 1);

            //kinect
            sensorChooser.KinectChanged += SensorChooserOnKinectChanged;
            sensorChooser.Start();

            mMainCamera = new Camera(get_window_dimensions());

            mPlayerHead = new PlayerHead(mHeadGraphics, this);

            ebHitPoints = new HealthBar(10, mHeartGraphics);

            eb_reset_game();
        }

        static void create_explosion(Vector2 position)
        {
            mEffectManager.add_effect(new Effect.EffectAnimatedGif(mExplosionGif, mMainCamera.transform_vector(position), new Vector2(300, 300)));
        }

        static void create_particle_explosion_A(Vector2 position)
        {
            mEffectManager.add_effect(new Effect.EffectExplodeParticle(mParticleATexture, mMainCamera.transform_vector(position), 400, .7f, 50, 50, new Vector2(0, 1500.0f)));
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            mDefaultFont = this.Content.Load<SpriteFont>("DefaultFont");
            mOpenTexture = this.Content.Load<Texture2D>("florence_open");
            mCloseTexture = this.Content.Load<Texture2D>("florence_closed");
            mBirdTexture = this.Content.Load<Texture2D>("bird");
            //mBiteSomethingSound = this.Content.Load<SoundEffect>("crunch");
            //mOpenMouthSound = this.Content.Load<SoundEffect>("inhale");
            mParticleATexture = this.Content.Load<Texture2D>("headmovingbiting/srpite-10");

            mImgs.Add("missile_ph", this.Content.Load<Texture2D>("missile_ph"));
            mSounds.Add("explosion_01", this.Content.Load<SoundEffect>("explosion_01"));
            mSounds.Add("Gah!", this.Content.Load<SoundEffect>("headmovingbiting/sfx/Gah!"));
            mSounds.Add("open_mouth", this.Content.Load<SoundEffect>("headmovingbiting/sfx/open_mouth"));
            mSounds.Add("close_mouth(no bird)", this.Content.Load<SoundEffect>("headmovingbiting/sfx/crush"));
            mSounds.Add("close_mouth(bird)", this.Content.Load<SoundEffect>("headmovingbiting/sfx/crush_bird"));
            mSounds.Add("bird_death2", this.Content.Load<SoundEffect>("headmovingbiting/sfx/bird_death2"));
            mSounds.Add("bird_death", this.Content.Load<SoundEffect>("headmovingbiting/sfx/bird_death"));

            mExplosionGif = Content.Load<GifAnimation.GifAnimation>("explosion1");

            mBirdGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-05"));
            mBirdGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-05"));
            mBirdGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-05"));
            mBirdGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-04"));
            mBirdGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-03"));
            mBirdGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-03"));
            mBirdGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-03"));

            mRocketGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-01"));
            mRocketGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-02"));

            mHeadGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-21"));
            mHeadGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-22"));
            mHeadGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-23"));
            mHeadGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-24"));
            mHeadGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-19"));
            mHeadGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-20"));

            mHeartGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-12"));
            mHeartGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-13"));
            mHeartGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-14"));
            mHeartGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-15"));
            mHeartGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-16"));

            mAnouncementGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-25"));
            mAnouncementGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-26"));
            mAnouncementGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-27"));
            mAnouncementGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-28"));
            mAnouncementGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-29")); //face the duck
            mAnouncementGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-30")); //lose
            mAnouncementGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-31")); //eat it
            mAnouncementGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-32")); //open

            mWarningGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-18"));
            mWarningGraphics.add_image(Content.Load<Texture2D>("headmovingbiting/srpite-17")); //TODDO this shoudl be empty image
        }

        protected override void UnloadContent() { }

        protected override void Update(GameTime gameTime)
        {

            //For debug kinect, need to call this function
            if (mKinect is KinectDataSansKinect)
            {
                ((KinectDataSansKinect)mKinect).handleKeyStrokes();
            }

            KeyboardState ks = Keyboard.GetState();
            if (ks.IsKeyDown(Keys.Escape))
            {
                this.Exit();
            }

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                this.Exit();
            }

            float keySpeed = 1.75f;

            if (ks.IsKeyDown(Keys.Left))
            {
                this.mPlayerHead.kinectMidpoint.X+= keySpeed;
            }

            if (ks.IsKeyDown(Keys.Right))
            {
                this.mPlayerHead.kinectMidpoint.X-= keySpeed;
            }

            if (ks.IsKeyDown(Keys.Up))
            {
                this.mPlayerHead.kinectMidpoint.Y += keySpeed;
            }

            if (ks.IsKeyDown(Keys.Down))
            {
                this.mPlayerHead.kinectMidpoint.Y-= keySpeed;
            }


            eb_update(gameTime);
            mEffectManager.update((float)gameTime.ElapsedGameTime.TotalSeconds);
            base.Update(gameTime);


        }
        public void eb_reset_game()
        {
            ebGameState = gameState.INTRO;
            ebHitPoints.setHealth(5);
            this.mScore = 0;
            this.mComboLevel = 0;

            mEdibles.Clear();
            FakeBird fb = new FakeBird(mBirdGraphics, this);
            fb.mPos = new Vector2(90, -100);
            addSprite(fb);

            setUpWaves();
        }

        private void setUpWaves()
        {
            mWaves.Clear();
            mWaves.Enqueue(new Wave1(this));
            mWaves.Enqueue(new HorizontalMissileSpam(this));
            mWaves.Enqueue(new Wave2(this));
            mWaves.Enqueue(new CircleMissileSpam(this));
            mWaves.Enqueue(new FinalWave(this));
        }

        public void eb_update(GameTime time)
        {

            bool haveFace = mKinect.getNTrackedPlayers() > 0 && mKinect.get_player(0) != null;

            if (haveFace)
            {
                KinectFace face = mKinect.get_player(0).get_kinect_face();

                // Z rotation is safe
                mDisplayHeadRotationWarning = (Math.Abs(face.Rotation.X) > 20) || (Math.Abs(face.Rotation.Y) > 30);
                //System.Diagnostics.Debug.Print("< " + face.Rotation.X + ", " + face.Rotation.Y + ", " + face.Rotation.Z + " >");
            }

            if (ebGameState == gameState.INTRO && haveFace)
            {
                mPlayerHead.update(time, mKinect, mEdibles);
            }
            else if (ebGameState == gameState.PLAYING && haveFace) // Wait for stable expression
            {

                //update monster head position
                mPlayerHead.update(time, mKinect, mEdibles);
                ebHitPoints.update(time);
                mMainCamera.update(time);

                if (mWaves.Count() == 0)
                {
                    this.gameSpeed += .5f;
                    setUpWaves();
                    //throw new Exception("no more waves!!!!");
                }

                Wave currentWave = mWaves.Peek();
                currentWave.update();
                if (currentWave.isFinished())
                {
                    mWaves.Dequeue();
                }
                //Generate enemies, if too few present
                /*if (mEdibles.Count() < 6)
                {
                    mEdibles.Add(new StupidBird());
                }

                bool missilePresent = false;
                foreach (Enemy e in mEdibles)
                {
                    missilePresent |= e is EnemyMissile;
                }

                if (!missilePresent)
                {
                    mEdibles.Add(new EnemyMissile());
                }*/

                ebTicksElapsed++;

                //Boolean mouthOpenPreviousFrame = mPlayerHead.mouthIsOpen;
                //mPlayerHead.mouthIsOpen = 

                List<Sprite> toRemove = new List<Sprite>();



                foreach (Sprite e in mEdibles)
                {
                    e.update(time);
                    if (e.dead)
                    {
                        toRemove.Add(e);
                    }
                }

                foreach (Sprite e in toRemove)
                {
                    mEdibles.Remove(e);
                }

                if (ebHitPoints.isDead())
                {
                    mGameOverDelay = TIME_GAME_OVER;
                    ebGameState = gameState.LOSE;
                    int score;
                    Texture2D tex;
                    get_score(out score, out tex);
                    if (mScore > score)
                    {
                        write_score(mScore);
                        snag_picture(true);
                    }
                    else snag_picture(false);
                    
                }
            }
            else if (ebGameState == gameState.LOSE) // Tell player what expression to make
            {
                mGameOverDelay -= time.ElapsedGameTime.TotalSeconds;

                if (mGameOverDelay < 0)
                {
                    eb_reset_game();
                    ebGameState = gameState.INTRO;
                }
            }
        }

        //public Boolean mouthOpen(int playerIndex)
        //{
        //    return ebGameState == gameState.PLAYING && HeadMovingBitingGame.mouthOpen(mKinect, playerIndex);//mKinect.get_player(playerIndex).get_kinect_face().get_smoothed_AUs()[1] > .3f;
        //}

        int score
        {
            get
            {
                return mScore;
            }

            set
            {
                this.mScore = value;
            }
        }

        int comboLevel
        {
            get
            {
                return this.mComboLevel;
            }

            set
            {
                this.mComboLevel = value;
            }
        }

        public void eb_draw()
        {
            Vector2 drawSize = new Vector2(200, 200);


            mEffectManager.draw(spriteBatch);

            foreach (Sprite e in mEdibles)
            {
                e.draw(mMainCamera);
                /*Vector2 loc = new Vector2(e.mPos.X + mFaceCenter.X + 20, e.mPos.Y + mFaceCenter.Y + 25);
                Vector2 dim = new Vector2(75,75);
                Rectangle rect = new Rectangle((int)(loc.X-dim.X/2),(int)(loc.Y-dim.Y/2),(int)(dim.X),(int)(dim.Y));
                String pelletAppearanceString = "";
                if (e.mResponseNeeded == Enemy.RequiredResponse.OPEN)
                {
                    pelletAppearanceString = "[O]";
                    spriteBatch.Draw(mImgOPEN, rect, Color.White);
                }
                else if (e.mResponseNeeded == Enemy.RequiredResponse.CHEW)
                {
                    pelletAppearanceString = "[()]";
                    spriteBatch.Draw(mImgCHEW, rect, Color.White);
                }
                else if (e.mResponseNeeded == Enemy.RequiredResponse.CLOSE)
                {
                    pelletAppearanceString = "{-}";
                    spriteBatch.Draw(mImgCLOSE, rect, Color.White);
                }

                spriteBatch.DrawString(mDefaultFont, pelletAppearanceString, new Vector2(e.mPos.X + mFaceCenter.X, e.mPos.Y + mFaceCenter.Y + 25), Color.Black);*/
            }
            mPlayerHead.draw(mMainCamera);
            ebHitPoints.draw(mMainCamera);



            spriteBatch.DrawString(mDefaultFont, "" + this.score, new Vector2(50, 100), Color.White,
                0, //rotation
                new Vector2(0, 0), //origin
                1, //scale
                SpriteEffects.None,
                0 //Sprite layer
                );
        }


        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);


            if (ebGameState == gameState.LOSE)
            {
                spriteBatch.Begin();

                Texture2D tex;
                Rectangle crop;
                mAnouncementGraphics.get_sprite_graphics(5, out crop, out tex);
                spriteBatch.Draw(tex, crop, Color.White);

                Texture2D hightex;
                int score;
                get_score(out score, out hightex);
                // TODO: Figure out placement and color
                spriteBatch.DrawString(mDefaultFont, "Final Score: " + mScore, new Vector2(100, 100), Color.White);
                spriteBatch.DrawString(mDefaultFont, "High Score: " + score, new Vector2(1920/2, 100), Color.White);
                if(hightex != null)
                    spriteBatch.Draw(hightex, new Rectangle(1800, 100, 100, 100), Color.White);
                if(mPlayerTexture != null)
                    spriteBatch.Draw(mPlayerTexture, new Rectangle(600, 100, 100, 100), Color.White);
                spriteBatch.End();

                base.Draw(gameTime);
                return;
            }

            int fontHeightIncrement = 30;
            int fontHeight = 10;
            spriteBatch.Begin();
            Texture2D t2d = mKinect.get_color_texture(graphics.GraphicsDevice);
            if (t2d != null)
            {
                spriteBatch.Draw(t2d,
                    new Rectangle(0 - (int)mMainCamera.Pos.X, 0 - (int)mMainCamera.Pos.Y,
                    (int)get_window_dimensions().X, (int)get_window_dimensions().Y), new Color(0f, .1f, .35f, 1));
                //spriteBatch.Draw(t2d, new Rectangle(0, 0, (int)get_window_dimensions().X, (int)get_window_dimensions().Y), new Color(.2f, .2f, .2f, 1));
                spriteBatch.DrawString(mDefaultFont, "Tracking working", new Vector2(10, fontHeight), Color.Black);
                fontHeight += fontHeightIncrement;
                spriteBatch.DrawString(mDefaultFont, "Faces found: " + mKinect.getNTrackedPlayers(), new Vector2(10, fontHeight), Color.Black);
            }

            for (int i = 0; i < 1; i++) //for each player
            {
                if (mKinect.get_player(i) != null)
                {
                    VertexPositionColor[] vertices = null;//mKinect.get_player(i).get_2d_face_model();
                    KinectFace playerFace = mKinect.get_player(i).get_kinect_face();
                    if (vertices != null)
                    {
                        fontHeight += fontHeightIncrement;
                        string s = "";
                        foreach (float f in playerFace.get_smoothed_AUs())
                            s += " " + (f >= 0 ? "+" : "") + f.ToString("0.00");
                        spriteBatch.DrawString(mDefaultFont, "AU: " + s, new Vector2(10, fontHeight), Color.Black);
                        mBasicEffect.CurrentTechnique.Passes[0].Apply();
                        graphics.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineStrip, vertices, 0, vertices.Length / 2);
                    }
                    //spriteBatch.DrawString(mDefaultFont, "P" + (i + 1) + "", playerFace.mCenter, Color.BlueViolet);
                }
            }


            eb_draw();

            if (mDisplayHeadRotationWarning)
            {
                Texture2D tex;
                Rectangle crop;
                mAnouncementGraphics.get_sprite_graphics(4, out crop, out tex);
                spriteBatch.Draw(tex, crop, Color.White);
            }

            //draw announcements
            if (mKinect.get_player(0) == null || !mKinect.get_player(0).get_kinect_face().mIsTracked)
            {
                if (mKinect.get_player(0) != null && !mKinect.get_player(0).get_kinect_face().mIsTracked)
                {
                    System.Diagnostics.Debug.Print("why not here");
                }
                Texture2D tex;
                Rectangle crop;
                mAnouncementGraphics.get_sprite_graphics(4, out crop, out tex);
                spriteBatch.Draw(tex, crop, Color.White);
            }
            else if (ebGameState == gameState.INTRO)
            {
                FakeBird bird = null;
                foreach (Sprite e in mEdibles)
                {
                    if (e is FakeBird)
                    {
                        bird = (FakeBird)e;
                        break;
                    }
                }
                Texture2D tex;
                Rectangle crop;
                mAnouncementGraphics.get_sprite_graphics(bird.mHasMouthBeenOpened ? 6 : 7, out crop, out tex);
                spriteBatch.Draw(tex, crop, Color.White);
            }
            spriteBatch.End();
            base.Draw(gameTime);

            SpriteFont scoreFont = this.Content.Load<SpriteFont>("DefaultFont");
            //SpriteEffects se = SpriteEffects.None;


        }


        public void render_message(SpriteBatch asb, SpriteFont asf, string amsg)
        {
            asb.DrawString(asf, amsg, new Vector2(100, 200), Color.Red);
        }

        private void SensorChooserOnKinectChanged(object sender, KinectChangedEventArgs kinectChangedEventArgs)
        {
            KinectSensor oldSensor = kinectChangedEventArgs.OldSensor;
            KinectSensor newSensor = kinectChangedEventArgs.NewSensor;

            if (oldSensor != null)
            {
                oldSensor.AllFramesReady -= mKinect.KinectSensorOnAllFramesReady;
                oldSensor.ColorStream.Disable();
                oldSensor.DepthStream.Disable();
                oldSensor.DepthStream.Range = DepthRange.Default;
                oldSensor.SkeletonStream.Disable();
                oldSensor.SkeletonStream.EnableTrackingInNearRange = false;
                oldSensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Default;
            }

            if (newSensor != null)
            {
                try
                {
                    newSensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);
                    newSensor.DepthStream.Enable(DepthImageFormat.Resolution320x240Fps30);
                    try
                    {
                        // This will throw on non Kinect For Windows devices.
                        newSensor.DepthStream.Range = DepthRange.Near;
                        newSensor.SkeletonStream.EnableTrackingInNearRange = true;
                    }
                    catch (InvalidOperationException)
                    {
                        newSensor.DepthStream.Range = DepthRange.Default;
                        newSensor.SkeletonStream.EnableTrackingInNearRange = false;
                    }

                    newSensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;
                    newSensor.SkeletonStream.Enable();
                    newSensor.AllFramesReady += mKinect.KinectSensorOnAllFramesReady;
                    mKinect.set_kinect(newSensor);
                }
                catch (InvalidOperationException)
                { }
            }
        }



        public static void drawPlus(Vector2 mPos)
        {
            /*
            BasicEffect mBasicEffect = new BasicEffect(GameHeadMovingBiting.spriteBatch.GraphicsDevice);
            mBasicEffect.VertexColorEnabled = true;
            mBasicEffect.Projection = Matrix.CreateOrthographicOffCenter
               (0, GameHeadMovingBiting.spriteBatch.GraphicsDevice.Viewport.Width,     // left, right
                GameHeadMovingBiting.spriteBatch.GraphicsDevice.Viewport.Height, 0,    // bottom, top
                0, 1);

            VertexPositionColor[] vertices = new VertexPositionColor[2];
            vertices[0] = new VertexPositionColor(new Vector3(mPos.X - 10, mPos.Y, 0), Color.HotPink);
            vertices[1] = new VertexPositionColor(new Vector3(mPos.X + 10, mPos.Y, 0), Color.HotPink);
            mBasicEffect.CurrentTechnique.Passes[0].Apply();
            GameHeadMovingBiting.spriteBatch.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, vertices, 0, 1);

            vertices[0] = new VertexPositionColor(new Vector3(mPos.X, mPos.Y - 10, 0), Color.HotPink);
            vertices[1] = new VertexPositionColor(new Vector3(mPos.X, mPos.Y + 10, 0), Color.HotPink);
            GameHeadMovingBiting.spriteBatch.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, vertices, 0, 1);
            */
        }

        // END MEMBER FUNCTIONS
        // ///////////////////////////////////////////////////////////////////////////////////////////////////////////

        // //////////////////////////////////////////////////////////////////////////////////////////////////////
        // STATIC FUNCTIONS
        // //////////////////////////////////////////////////////////////////////////////////////////////////////

        public static Vector2 get_window_dimensions()
        {
            return new Vector2(1920, 1080);
        }
        public static Vector2 get_windows_center()
        {
            return get_window_dimensions() / 2;
        }

    }
}
